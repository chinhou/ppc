﻿
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size);

__global__ void addKernel(int *c, const int *a, const int *b)
{
    //int i = threadIdx.x;
    //c[i] = a[i] + b[i];
}

#define WARP_SIZE 32
#define REGISTER_ARRAY_SIZE 2
#define FILTER_SIZE 1
__global__ void kstencilShuffle(
	int* input,
	int* output,
	int size) {
	int threadInput[REGISTER_ARRAY_SIZE];
	int threadOutput = 0, reg_idx, tid_idx;
	int lindex = threadIdx.x&(WARP_SIZE - 1);
	int gindex = threadIdx.x + blockIdx.x*blockDim.x;

	//PREFETCH.note:inispaddedbyFILTER_SIZE
	int lowIdx = gindex - FILTER_SIZE;
	int highIdx = lowIdx + WARP_SIZE;
	threadInput[0] = input[lowIdx];
	threadInput[1] = input[highIdx];

	//Firstiteration􀀀dataavailablelocally
	threadOutput += threadInput[0];
	//COMMUNICATE+COMPUTE
	reg_idx = (lindex == 0) ? 1 : 0;
	tid_idx = (lindex + 1)&(WARP_SIZE - 1);
	int x = __shfl(threadInput[reg_idx], tid_idx);
	threadOutput += x;

	//COMMUNICATE+COMPUTE
	reg_idx = (lindex == 0 || lindex == 1) ? 1 : 0;
	tid_idx = (lindex + 2)&(WARP_SIZE - 1);
	int y = __shfl(threadInput[reg_idx], tid_idx);
	threadOutput += y;

	output[gindex] = threadOutput / FILTER_SIZE;

}

int main()
{
    const int arraySize = 32*4;
	int size = arraySize;
    static int a[arraySize] = { 1, 2, 3, 4, 5 };
	for (int i = 0; i < arraySize; i++) a[i] = i + 1;
	static int b[arraySize] = { 0, };

	int *dev_a = 0;
	int *dev_b = 0;
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}

	// Copy input vectors from host memory to GPU buffers.
	cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}


	// Launch a kernel on the GPU with one thread for each element.
	// addKernel << <1, size >> >(dev_c, dev_a, dev_b);
	kstencilShuffle << <1, arraySize >> > (dev_a, dev_b, size);


	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(b, dev_b, size * sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	for (int i = 0; i < size; i++)
		printf("%d, ", b[i]);



Error:
	cudaFree(dev_a);
	cudaFree(dev_b);
    return 0;
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t addWithCuda(int *c, const int *a, const int *b, unsigned int size)
{
    int *dev_a = 0;
    int *dev_b = 0;
    int *dev_c = 0;
    cudaError_t cudaStatus;

    // Choose which GPU to run on, change this on a multi-GPU system.
    cudaStatus = cudaSetDevice(0);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
        goto Error;
    }

    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMalloc failed!");
        goto Error;
    }

    // Copy input vectors from host memory to GPU buffers.
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<1, size>>>(dev_c, dev_a, dev_b);

    // Check for any errors launching the kernel
    cudaStatus = cudaGetLastError();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
        goto Error;
    }
    
    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
        goto Error;
    }

    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);
    
    return cudaStatus;
}
