
#include "../conv/common.h"
#include "../conv/cudaLib.cuh"
#include <stdio.h>

const int WARP_SIZE = 32;
template<typename T, int N>
__global__ void kernelSmem(const T* src, T* dst, float* avgTime, int size)
{
	__shared__ T sMem[1024];
	const int laneId = threadIdx.x % WARP_SIZE;
	const int warpId = threadIdx.x / WARP_SIZE;
	const int tid = threadIdx.x + blockDim.x*blockIdx.x;
	sMem[threadIdx.x] = src[tid];

	T* pMem = &sMem[warpId*WARP_SIZE];

	if (laneId == 0) {
		int start = clock();
#pragma unroll
		for (int i = 0; i < N; i++) {
#pragma unroll
			for (int j = 0; j < WARP_SIZE - 1; j++) {
				pMem[j + 1] = pMem[j];
			}
#pragma unroll
			for (int j = WARP_SIZE - 2; j >= 0; j--) {
				pMem[j] = pMem[j+1];
			}
		}
		int end = clock();
		avgTime[tid/WARP_SIZE] = (end - start) * 1000 / float(N*(WARP_SIZE - 1) * 2);
	}
	dst[tid] = sMem[threadIdx.x];
}

template<typename T, int N>
__global__ void kernelRegister(const T* src, T* dst, float* avgTime, int size)
{
	const int laneId = threadIdx.x % WARP_SIZE;
	const int warpId = threadIdx.x / WARP_SIZE;
	const int tid = threadIdx.x + blockDim.x*blockIdx.x;
	T data = src[tid];
	if (laneId == 0) {
		int start = clock();
#pragma unroll
		for (int i = 0; i < N; i++) {
#pragma unroll
			for (int j = 0; j < WARP_SIZE - 1; j++) {
				__my_shfl_up(data, 1);
			}
#pragma unroll
			for (int j = WARP_SIZE - 2; j >= 0; j--) {
				__my_shfl_down(data, 1);
			}
		}
		int end = clock();
		avgTime[tid / WARP_SIZE] = (end - start)*1000 / float(N*(WARP_SIZE - 1) * 2);
	}
	dst[tid] = data;
}




template<int N>
int testMain(int size = 1024 * 1024 * 256)
{
	printf("N = %d\n", N);
	//int size = 1024 * 1024 * 256;
	DevData<float> src(size), dst(size), avgTm(size / WARP_SIZE);
	dim3 blocks(1024);
	dim3 grids(UpDivide(size, blocks.x));
	kernelSmem<float, N><<<grids, blocks>>>(src.GetData(), dst.GetData(), avgTm.GetData(), size);
	cudaDeviceSynchronize();

	DataT<float> data;
	data.MallocBuffer(avgTm.width, 1, 1, 1);
	avgTm.CopyToHost(data.data, data.width, data.width, 1, 1);

	float avg1 = 0;
	for (int i = 0; i < avgTm.width; i++) {
		avg1 += data.data[i];
		//printf("%f ", data.data[i]);
	}
	avg1 /= avgTm.width;
	printf("smem avg = %f\n", avg1);
	printf("-----------------------\n");


	kernelRegister<float, N> << <grids, blocks >> >(src.GetData(), dst.GetData(), avgTm.GetData(), size);
	cudaDeviceSynchronize();

	//DataT<float> data(avgTm.width, 1, 1, 1);
	avgTm.CopyToHost(data.data, data.width, data.width, 1, 1);

	float avg2 = 0;
	for (int i = 0; i < avgTm.width; i++) {
		avg2 += data.data[i];
		//printf("%f ", data.data[i]);
	}
	avg2 /= avgTm.width;
	printf("reg avg = %f\n", avg2);
	printf("times = %f\n", avg1/avg2);
	printf("-----------------------\n");

    return 0;
}

int main(int argc, char** argv) {
	const int size = 1024 * 1024 * 256;
	for (int i = 1; i < 10; i++) {
		testMain<16 * 1>(size*1);
		testMain<16 * 2>(size * 1);
		testMain<16 * 3>(size * 1);
		testMain<16 * 4>(size * 1);
		testMain<16 * 5>(size * 1);
		testMain<16 * 6>(size * 1);
	}
	return 0;
}
