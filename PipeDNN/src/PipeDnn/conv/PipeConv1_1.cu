#include "cudalib.cuh"
#include "common.h"
#include "StopWatch.h"
//#include "cuda_common.cuh"

namespace Conv1_1 {
	static const int WARP_SIZE = 32;

#define PTX_FMAD(A, B, C, D)    asm volatile("mad.rz.ftz.f32 %0,%1,%2,%3;" : "=f"(A) : "f"(B), "f"(C), "f"(D));
	__forceinline__ __device__ void __FMAD(float& _w, float _x, float _y, float _z)
	{
		asm volatile("mad.rz.ftz.f32 %0,%1,%2,%3;" : "=f"(_w) : "f"(_x), "f"(_y), "f"(_z));
		//PTX_FMAD(_w, _x, _y, _z);
	}

	template<typename T, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT, int FILTER_CHANNELS, int FILTER_NUMBER, int BLOCK_DIM_X>
	__global__ void kerenlOneBlockOneChannel(const T* __restrict__ src, T* dst, int width, int widthStride, int height, int channels,
		const T* __restrict__ filter, int fx, int fy, int fc, int fn, int padw, int padh)
	{
		assert(FILTER_NUMBER == fn);
		assert(FILTER_CHANNELS == fc);

		const int MAX_FILTER_COUNT = 32;
		const int REG_COUNT = PROCESS_DATA_COUNT*MAX_FILTER_COUNT + FILTER_HEIGHT;
		const int WARP_COUNT = BLOCK_DIM_X >> 5;
		const int BLOCK_DATA_X = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_DATA_Y = PROCESS_DATA_COUNT;
		const int DST_OFFSET = FILTER_WIDTH - 1;
		assert(BLOCK_DIM_X == blockDim.x);
		assert(REG_COUNT > FILTER_HEIGHT);
		//assert(blockDim.y == 1);
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;

		int tidx = laneId + BLOCK_DATA_X*blockIdx.x;
		int tidy = threadIdx.y + BLOCK_DATA_Y*blockIdx.y;
		assert(MAX_FILTER_COUNT == FILTER_NUMBER);

		__shared__ T smem[MAX_FILTER_COUNT][FILTER_CHANNELS][FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0][0][0];
		const int filter_size = MAX_FILTER_COUNT*FILTER_CHANNELS*FILTER_HEIGHT*FILTER_WIDTH;
#pragma unroll	
		for (int i = threadIdx.x; i < filter_size; i += blockDim.x) {
			if (i < filter_size) {
				psmem[i] = filter[i];
			}
		}
		__syncthreads();

		T data[FILTER_HEIGHT];
		T result[PROCESS_DATA_COUNT*MAX_FILTER_COUNT];

		int index = tidx + widthStride*tidy + warpId*widthStride*height;
#pragma unroll
		for (int s = 0; s < FILTER_HEIGHT; s++) {
			if (tidy + s < height && tidx < width)
				data[s] = src[index];
			else
				data[s] = 0;
			index += widthStride;
		}
		//T* pData = data;
		__shared__ T sResult[MAX_FILTER_COUNT][WARP_SIZE];

		//T sum = 0;
#pragma unroll
		for (int i = 0; i < MAX_FILTER_COUNT; i++) {
			T sum = 0;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0)
					sum = __shfl_up(sum, 1);
				//int a = data[i + 0];
				//int b = data[i + 1];
				//int c = data[i + 2];
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
					sum += data[n] * smem[i][warpId][n][m];
					//sum = Mad(data[i + n], smem[n][m], sum);
					//__FMAD(sum, data[i + n], smem[i][warpId][n][m], sum);
				}
			}
			result[i] = sum;
			//result[i] = __shfl_down(sum, FILTER_WIDTH/2);
			//if (i + tidy < height) {
			//	dst[index] = sum;
			//	index += widthStride;
			//}
		}
		//T* p = result;

		//shared memory set 0
#pragma unroll
		for (int i = warpId; i < MAX_FILTER_COUNT; i += WARP_COUNT) {
			sResult[i][laneId] = 0;
		}
		__syncthreads();

#pragma unroll
		for (int s = 0; s < MAX_FILTER_COUNT; s++) {
			int idx = (s + warpId) % MAX_FILTER_COUNT;
			//if (idx >= MAX_FILTER_COUNT) idx -= MAX_FILTER_COUNT;
			//int a = result[idx];
			sResult[idx][laneId] += result[idx];
			__syncthreads();
		}

		if (laneId < WARP_SIZE - DST_OFFSET && tidx < width && tidy < height) {
			index = tidx + widthStride*tidy + warpId*widthStride*height;
#pragma unroll
			for (int i = warpId; i < MAX_FILTER_COUNT; i += WARP_COUNT) {
				dst[index] = sResult[i][laneId + DST_OFFSET];
				index += widthStride*height*WARP_COUNT;
			}
		}
	}

	static void TestOneBlockOneChannel() {
		typedef float DataType;
		const int width = 514;
		const int height = 514;
		const int channels = 3;

		const int filter_width = 3;
		const int filter_height = 3;
		const int filter_channels = 3;
		const int filter_number = 32;

		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> src, filter, dst;
		char szPath[1024] = "";
		src.MallocBuffer(width, height, channels, 1);
		filter.MallocBuffer(filter_width, filter_height, channels, filter_number);
		dst.MallocBuffer(width, height, filter.count, 1);

		for (int i = 0; i < src.Size(); i++) {
			//src.data[i] = i + 1;
			src.data[i] = 1;
		}
		for (int i = 0; i < filter.Size(); i++) {
			//filter.data[i] = i + 1;
			filter.data[i] = 1;
		}

		//sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		//bool bRtn = img.Load<uchar>(szPath, width, height);
		//if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(src.width, src.height, src.channels), devDst(dst.width, dst.height, dst.channels), devFilter(filter.Size());
		devSrc.CopyFromHost(src.data, src.width, src.width, src.height, src.channels);
		devFilter.CopyFromHost(filter.data, filter.Size(), filter.Size(), 1);

		const int PROCESS_DATA_COUNT = 1;
		const int FILTER_WIDTH = filter_width;
		const int FILTER_HEIGHT = filter_height;
		const int FILTER_CHANNELS = filter_channels;
		const int FILTER_NUMBER = filter_number;
		const int BLOCK_DIM_X = WARP_SIZE*channels;

		dim3 block_size(BLOCK_DIM_X, 1);

		const int BLOCK_DATA_X = (BLOCK_DIM_X / WARP_SIZE * (WARP_SIZE - 2));

		dim3 grid_size(UpDivide(width - FILTER_WIDTH / 2 * 2, WARP_SIZE - FILTER_WIDTH / 2 * 2), UpDivide(height, PROCESS_DATA_COUNT) - FILTER_HEIGHT / 2 * 2);

		cudaEventRecord(start, 0);
		kerenlOneBlockOneChannel<DataType, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT, FILTER_CHANNELS, FILTER_NUMBER, BLOCK_DIM_X> << <grid_size, block_size >> >
			(devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, channels, devFilter.GetData(), filter.width, filter.height, filter.channels, filter.count, filter.width / 2, filter.height / 2);
		cudaDeviceSynchronize();
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(dst.data, dst.width, dst.width, dst.height, dst.channels);
		CUDA_CHECK_ERROR;

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		printf("(%dx%dx%d) * (%dx%dx%dx%d) , %f ms , %f fps\n", width, height, channels, filter.width, filter.height, filter.channels, filter.count, inc, 1000.0 / inc);

		sprintf(szPath, "../data/data%dx%dx%d.txt", width, dst.width, dst.height, dst.channels);
		dst.SaveText(szPath);
	}
};
void Test_PipConv3() {
	Conv1_1::TestOneBlockOneChannel();
}