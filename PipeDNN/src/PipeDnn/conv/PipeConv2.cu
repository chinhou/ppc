#include "cudalib.cuh"
#include "common.h"
#include "StopWatch.h"
//#include "cuda_common.cuh"

static const int WARP_SIZE = 32;
namespace Conv2 {
	template<typename T, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT, int BLOCK_DIM_X>
	__global__ void kerenlOneBlockOneChannel(int BLOCK_NUM_X, const T* __restrict__ src, T* dst, int width, int widthStride, int height, int channels,
		const T* __restrict__ filter, int fx, int fy, int fc, int fn, int padw, int padh)
	{

		const int MAX_FILTER_COUNT = 32;
		const int REG_COUNT = PROCESS_DATA_COUNT*MAX_FILTER_COUNT + FILTER_HEIGHT;
		const int WARP_COUNT = BLOCK_DIM_X >> 5;
		const int BLOCK_DATA_X = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_DATA_Y = PROCESS_DATA_COUNT;
		const int DST_OFFSET = FILTER_WIDTH - 1;
		const int GRID_DIM_X = gridDim.x;
		const int GRID_DIM_Y = gridDim.y;
		assert(BLOCK_DIM_X == blockDim.x);
		assert(REG_COUNT > FILTER_HEIGHT);
		//assert(blockDim.y == 1);
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;

		//assert(BLOCK_NUM_X == MAX_FILTER_COUNT);
		int BLOCK_IDX_X = blockIdx.x % BLOCK_NUM_X;
		int BLOCK_IDX_Y = blockIdx.x / BLOCK_NUM_X;

		int tidx = laneId + BLOCK_DATA_X*BLOCK_IDX_X;
		int tidy = threadIdx.y + BLOCK_DATA_Y*blockIdx.y;

		__shared__ T smem[3][FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0][0];

		int filter_size = FILTER_WIDTH*FILTER_HEIGHT * 3;
		int offset = BLOCK_IDX_Y*filter_size;
		//for (int i = threadIdx.x; i < filter_size; i += blockDim.x) {
		//	if (i < filter_size) {
		//		psmem[i] = filter[i + offset];
		//	}
		//}
		if (threadIdx.x < filter_size) {
			psmem[threadIdx.x] = filter[threadIdx.x + offset];
		}
		__syncthreads();

		T data[FILTER_HEIGHT];
		T result[PROCESS_DATA_COUNT*MAX_FILTER_COUNT];

		int index = tidx + widthStride*tidy + warpId*widthStride*height;
#pragma unroll
		for (int s = 0; s < FILTER_HEIGHT; s++) {
			if (tidy + s < height && tidx < width)
				data[s] = src[index];
			else
				data[s] = 0;
			index += widthStride;
		}
		T* pData = data;

		//#pragma unroll
			//const int i = 0;
			//for (int i = 0; i < fn; i++) 
		{
			T sum = 0;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0)
					sum = __shfl_up(sum, 1);
				int a = data[0];
				int b = data[1];
				int c = data[2];
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
					sum += data[n] * smem[warpId][n][m];
					//sum = Mad(data[i + n], smem[n][m], sum);
				}
			}
			result[0] = sum;
			//result[i] = __shfl_down(sum, FILTER_WIDTH/2);
			//if (i + tidy < height) {
			//	dst[index] = sum;
			//	index += widthStride;
			//}
		}
		int* p = result;

		const int S_WARP_COUNT = (WARP_COUNT + 1) / 2;
		__shared__ T sResult[S_WARP_COUNT][WARP_SIZE];
		if (warpId < S_WARP_COUNT) {
			sResult[warpId][laneId] = result[0];
		}
		__syncthreads();
		if (warpId >= S_WARP_COUNT) {
			sResult[warpId - S_WARP_COUNT][laneId] += result[0];
		}
		__syncthreads();
#pragma unroll
		for (int total = S_WARP_COUNT, half = (total + 1) >> 1; total >= 2; total = half, half = (total + 1) >> 1) {
			if (warpId < half) {
				int idx = warpId + half;
				if (idx < total) {
					sResult[warpId][laneId] += sResult[idx][laneId];
				}
			}
			__syncthreads();
		}
		/*
			//shared memory set 0
			#pragma unroll
			for (int i = warpId; i < MAX_FILTER_COUNT; i += WARP_COUNT) {
					sResult[i][laneId] = 0;
			}
			__syncthreads();

			#pragma unroll
			for (int s = 0; s < MAX_FILTER_COUNT; s++) {
				int idx = s + warpId;
				if (idx >= MAX_FILTER_COUNT) idx -= MAX_FILTER_COUNT;
				int a = result[idx];
				sResult[idx][laneId] += result[idx];
				__syncthreads();
			}
		*/
		//if (BLOCK_IDX_Y > 0 || tidy > 0) return;
		index = tidx + widthStride*tidy + BLOCK_IDX_Y*widthStride*height;
		if (warpId == 0 && laneId < WARP_SIZE - DST_OFFSET && tidx < width && tidy < height)
		{
			dst[index] = sResult[0][laneId + DST_OFFSET];
			//for (int i = warpId; i < fn; i += WARP_COUNT) {
			//	dst[index] = sResult[i][laneId + DST_OFFSET];
			//	index += widthStride*height*WARP_COUNT;
			//}
		}
	}

	static void TestOneBlockOneChannel() {
		typedef int DataType;
		const int width = 514;
		const int height = 514;
		const int channels = 3;

		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> src, filter, dst;
		char szPath[1024] = "";
		src.MallocBuffer(width, height, channels, 1);
		filter.MallocBuffer(3, 3, channels, 32);
		dst.MallocBuffer(width, height, filter.count, 1);

		for (int i = 0; i < src.Size(); i++) {
			//src.data[i] = i + 1;
			src.data[i] = 1;
		}
		for (int i = 0; i < filter.Size(); i++) {
			filter.data[i] = i + 1;
			//filter.data[i] = 1;
		}

		//sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		//bool bRtn = img.Load<uchar>(szPath, width, height);
		//if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(src.width, src.height, src.channels), devDst(dst.width, dst.height, dst.channels), devFilter(filter.Size());
		devSrc.CopyFromHost(src.data, src.width, src.width, src.height, src.channels);
		devFilter.CopyFromHost(filter.data, filter.Size(), filter.Size(), 1);

		const int PROCESS_DATA_COUNT = 1;
		const int FILTER_WIDTH = 3;
		const int FILTER_HEIGHT = 3;
		const int BLOCK_DIM_X = WARP_SIZE*channels;
		const int BLOCK_NUM_X = filter.count;

		dim3 block_size(BLOCK_DIM_X, 1);

		const int BLOCK_DATA_X = (BLOCK_DIM_X / WARP_SIZE * (WARP_SIZE - 2));

		dim3 grid_size(UpDivide(width - FILTER_WIDTH / 2 * 2, WARP_SIZE - FILTER_WIDTH / 2 * 2)*BLOCK_NUM_X, UpDivide(height, PROCESS_DATA_COUNT) - FILTER_HEIGHT / 2 * 2);
		printf("%d %d, %d %d\n", grid_size.x, grid_size.y, block_size.x, block_size.y);
		cudaEventRecord(start, 0);
		kerenlOneBlockOneChannel<DataType, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT, BLOCK_DIM_X> << <grid_size, block_size >> >
			(grid_size.x / BLOCK_NUM_X, devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, channels, devFilter.GetData(), filter.width, filter.height, filter.channels, filter.count, filter.width / 2, filter.height / 2);
		cudaDeviceSynchronize();
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(dst.data, dst.width, dst.width, dst.height, dst.channels);
		CUDA_CHECK_ERROR;

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		printf("%dx%dx%d , %f ms , %f fps\n", width, height, channels, inc, 1000.0 / inc);

		sprintf(szPath, "../data/data%dx%dx%d.txt", width, dst.width, dst.height, dst.channels);
		dst.SaveText(szPath);
	}
};

void Test_PipConv2() {
	Conv2::TestOneBlockOneChannel();
}