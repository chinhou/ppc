#include "cudalib.cuh"
#include "common.h"
#include "StopWatch.h"
//#include "cuda_common.cuh"

static const int WARP_SIZE = 32;
namespace Conv2_1 {
	template<typename T, int PROCESS_DATA_COUNT, int PROCESS_FILTER_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT, int FILTER_CHANNELS, int FILTER_NUMBER, int BLOCK_DIM_X>
	__global__ void kerenlOneBlockOneChannel(int BLOCK_NUM_X, const T* __restrict__ src, T* dst, int width, int widthStride, int height, int channels,
		const T* __restrict__ filter, int fx, int fy, int fc, int fn, int padw, int padh)
	{
		const int MAX_FILTER_COUNT = FILTER_NUMBER;
		//const int REG_COUNT = PROCESS_DATA_COUNT*MAX_FILTER_COUNT + FILTER_HEIGHT;
		const int WARP_COUNT = BLOCK_DIM_X >> 5;
		const int BLOCK_DATA_X = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_DATA_Y = PROCESS_DATA_COUNT;
		const int DST_OFFSET = FILTER_WIDTH - 1;
		const int GRID_DIM_X = gridDim.x;
		const int GRID_DIM_Y = gridDim.y;
		assert(BLOCK_DIM_X == blockDim.x);
		//assert(REG_COUNT > FILTER_HEIGHT);
		//assert(blockDim.y == 1);
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;

		//assert(BLOCK_NUM_X == MAX_FILTER_COUNT);
		int BLOCK_IDX_X = blockIdx.x % BLOCK_NUM_X;
		int BLOCK_IDX_Y = blockIdx.x / BLOCK_NUM_X;

		int tidx = laneId + BLOCK_DATA_X*BLOCK_IDX_X;
		int tidy = threadIdx.y + BLOCK_DATA_Y*blockIdx.y;

		__shared__ T smem[PROCESS_FILTER_COUNT][FILTER_CHANNELS][FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0][0][0];

		const int filter_size = FILTER_WIDTH*FILTER_HEIGHT * 3;
		const int offset = BLOCK_IDX_Y*filter_size;
		//for (int i = threadIdx.x; i < filter_size; i += blockDim.x) {
		//	if (i < filter_size) {
		//		psmem[i] = filter[i + offset];
		//	}
		//}
		if (threadIdx.x < filter_size) {
			psmem[threadIdx.x] = filter[threadIdx.x + offset];
		}
		__syncthreads();

		register T data[FILTER_HEIGHT + PROCESS_DATA_COUNT - 1];
		register T result[PROCESS_DATA_COUNT];

		int index = tidx + widthStride*tidy + warpId*widthStride*height;
#pragma unroll
		for (int s = 0; s < FILTER_HEIGHT + PROCESS_DATA_COUNT - 1; s++) {
			if (tidy + s < height && tidx < width)
				data[s] = src[index];
			else
				data[s] = 0;
			index += widthStride;
		}
		T* pData = data;

#pragma unroll
		for (int s = 0; s < PROCESS_FILTER_COUNT; s++) {
#pragma unroll
			for (int i = 0; i < PROCESS_DATA_COUNT; i++)
			{
				T sum = 0;
#pragma unroll
				for (int m = 0; m < FILTER_WIDTH; m++) {
					if (m > 0)
						sum = __shfl_up(sum, 1);
					//int a = data[0];
					//int b = data[1];
					//int c = data[2];
#pragma unroll
					for (int n = 0; n < FILTER_HEIGHT; n++) {
						sum += data[n + i] * smem[s][warpId][n][m];
						//sum = Mad(data[i + n], smem[n][m], sum);
					}
				}
				result[i] = sum;
				//result[i] = __shfl_down(sum, FILTER_WIDTH/2);
				//if (i + tidy < height) {
				//	dst[index] = sum;
				//	index += widthStride;
				//}
			}
		}
		T* p = result;
		__shared__ T sResult[PROCESS_DATA_COUNT][WARP_SIZE + 1];

		//shared memory set 0
#pragma unroll
		for (int i = warpId; i < PROCESS_DATA_COUNT; i += WARP_COUNT) {
			sResult[i][laneId] = 0;
		}
		__syncthreads();

#pragma unroll
		for (int s = 0; s < PROCESS_DATA_COUNT; s++) {
			int idx = (s + warpId) % PROCESS_DATA_COUNT;
			//int a = result[idx];
			sResult[idx][laneId] += result[idx];
			__syncthreads();
		}

		//if (BLOCK_IDX_Y > 0 || tidy > 0) return;

		if (laneId < WARP_SIZE - DST_OFFSET && tidx < width/* && tidy < height */)
		{
			index = tidx + widthStride*(tidy + warpId) + BLOCK_IDX_Y*widthStride*height;
#pragma unroll
			for (int i = warpId; i < PROCESS_DATA_COUNT; i += WARP_COUNT) {
				if (i + tidy < height) {
					dst[index] = sResult[i][laneId + DST_OFFSET];
					index += widthStride*WARP_COUNT;
				}
				else {
					break;
				}
			}

			//dst[index] = sResult[0][laneId + DST_OFFSET];
			//for (int i = warpId; i < fn; i += WARP_COUNT) {
			//	dst[index] = sResult[i][laneId + DST_OFFSET];
			//	index += widthStride*height*WARP_COUNT;
			//}
		}
	}
#if 1
	template<>
	__global__ void kerenlOneBlockOneChannel<float, 8, 1, 3, 3, 3, 32, 96>(int BLOCK_NUM_X, const float* __restrict__ src, float* dst, int width, int widthStride, int height, int channels,
		const float* __restrict__ filter, int fx, int fy, int fc, int fn, int padw, int padh)
	{
		typedef float T;
		const int PROCESS_DATA_COUNT = 8;
		const int PROCESS_FILTER_COUNT = 1;
		const int FILTER_WIDTH = 3;
		const int FILTER_HEIGHT = 3;
		const int FILTER_CHANNELS = 3;
		const int FILTER_NUMBER = 32;
		const int BLOCK_DIM_X = 96;

		const int MAX_FILTER_COUNT = FILTER_NUMBER;
		//const int REG_COUNT = PROCESS_DATA_COUNT*MAX_FILTER_COUNT + FILTER_HEIGHT;
		const int WARP_COUNT = BLOCK_DIM_X >> 5;
		const int BLOCK_DATA_X = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_DATA_Y = PROCESS_DATA_COUNT;
		const int DST_OFFSET = FILTER_WIDTH - 1;
		const int GRID_DIM_X = gridDim.x;
		const int GRID_DIM_Y = gridDim.y;
		assert(BLOCK_DIM_X == blockDim.x);

		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;

		const int BLOCK_IDX_X = blockIdx.x % BLOCK_NUM_X;
		const int BLOCK_IDX_Y = blockIdx.x / BLOCK_NUM_X;

		int tidx = laneId + BLOCK_DATA_X*BLOCK_IDX_X;
		int tidy = threadIdx.y + BLOCK_DATA_Y*blockIdx.y;

		__shared__ T smem[PROCESS_FILTER_COUNT][FILTER_CHANNELS][FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0][0][0];

		const int filter_size = FILTER_WIDTH*FILTER_HEIGHT * 3;
		const int offset = BLOCK_IDX_Y*filter_size;

		if (threadIdx.x < filter_size) {
			psmem[threadIdx.x] = filter[threadIdx.x + offset];
		}
		__syncthreads();

		//T data[FILTER_HEIGHT + PROCESS_DATA_COUNT - 1];
		//T result[PROCESS_DATA_COUNT];

		T data0, data1, data2, data3, data4, data5, data6, data7, data8, data9;
		T result0, result1, result2, result3, result4, result5, result6, result7;

		int index = tidx + widthStride*tidy + warpId*widthStride*height;

		{ const int s = 0; if (tidy + s < height && tidx < width)  data0  = src[index]; else data0  = 0; index += widthStride; }
		{ const int s = 1; if (tidy + s < height && tidx < width)  data1  = src[index]; else data1  = 0; index += widthStride; }
		{ const int s = 2; if (tidy + s < height && tidx < width)  data2  = src[index]; else data2  = 0; index += widthStride; }
		{ const int s = 3; if (tidy + s < height && tidx < width)  data3  = src[index]; else data3  = 0; index += widthStride; }
		{ const int s = 4; if (tidy + s < height && tidx < width)  data4  = src[index]; else data4  = 0; index += widthStride; }
		{ const int s = 5; if (tidy + s < height && tidx < width)  data5  = src[index]; else data5  = 0; index += widthStride; }
		{ const int s = 6; if (tidy + s < height && tidx < width)  data6  = src[index]; else data6  = 0; index += widthStride; }
		{ const int s = 7; if (tidy + s < height && tidx < width)  data7  = src[index]; else data7  = 0; index += widthStride; }
		{ const int s = 8; if (tidy + s < height && tidx < width)  data8  = src[index]; else data8  = 0; index += widthStride; }
		{ const int s = 9; if (tidy + s < height && tidx < width)  data9  = src[index]; else data9  = 0; index += widthStride; }

		{
			const int s = 0;
			{
				const int i = 0;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data0 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data2 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data0 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data2 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data0 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data2 * smem[s][warpId][n][m]; }
				}
				result0 = sum;
			}
			{
				const int i = 1;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data3 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data3 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data3 * smem[s][warpId][n][m]; }
				}
				result1 = sum;
			}
			{
				const int i = 2;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data4 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data4 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data4 * smem[s][warpId][n][m]; }
				}
				result2 = sum;
			}
			{
				const int i = 3;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data5 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data5 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data5 * smem[s][warpId][n][m]; }
				}
				result3 = sum;
			}
			{
				const int i = 4;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data6 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data6 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data6 * smem[s][warpId][n][m]; }
				}
				result6 = sum;
			}
			{
				const int i = 5;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data7 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data7 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data7 * smem[s][warpId][n][m]; }
				}
				result5 = sum;
			}
			{
				const int i = 6;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data8 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data8 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data8 * smem[s][warpId][n][m]; }
				}
				result6 = sum;
			}
			{
				const int i = 7;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data8 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data9 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data8 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data9 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data8 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data9 * smem[s][warpId][n][m]; }
				}
				result7 = sum;
			}
		}
		__shared__ T sResult[PROCESS_DATA_COUNT][WARP_SIZE];

		assert(WARP_COUNT == 3);
		if (warpId == 0) 
		{
			{ const int i = 0; sResult[i][laneId] = result0; }
			{ const int i = 1; sResult[i][laneId] = result1; }
			{ const int i = 2; sResult[i][laneId] = result2; }
			{ const int i = 3; sResult[i][laneId] = result3; }
			{ const int i = 4; sResult[i][laneId] = result4; }
			{ const int i = 5; sResult[i][laneId] = result5; }
			{ const int i = 6; sResult[i][laneId] = result6; }
			{ const int i = 7; sResult[i][laneId] = result7; }
		}
		__syncthreads();
		if (warpId == 1)
		{
			{ const int i = 0; sResult[i][laneId] += result0; }
			{ const int i = 1; sResult[i][laneId] += result1; }
			{ const int i = 2; sResult[i][laneId] += result2; }
			{ const int i = 3; sResult[i][laneId] += result3; }
			{ const int i = 4; sResult[i][laneId] += result4; }
			{ const int i = 5; sResult[i][laneId] += result5; }
			{ const int i = 6; sResult[i][laneId] += result6; }
			{ const int i = 7; sResult[i][laneId] += result7; }
		}
		__syncthreads();
		if (warpId == 2)
		{
			{ const int i = 0; sResult[i][laneId] += result0; }
			{ const int i = 1; sResult[i][laneId] += result1; }
			{ const int i = 2; sResult[i][laneId] += result2; }
			{ const int i = 3; sResult[i][laneId] += result3; }
			{ const int i = 4; sResult[i][laneId] += result4; }
			{ const int i = 5; sResult[i][laneId] += result5; }
			{ const int i = 6; sResult[i][laneId] += result6; }
			{ const int i = 7; sResult[i][laneId] += result7; }
		}
		__syncthreads();
#if 1
		//if (BLOCK_IDX_Y > 0 || tidy > 0) return;

		if (laneId < WARP_SIZE - DST_OFFSET && tidx < width/* && tidy < height */)
		{
			index = tidx + widthStride*(tidy + warpId) + BLOCK_IDX_Y*widthStride*height;
//#pragma unroll
			for (int i = warpId; i < PROCESS_DATA_COUNT; i += WARP_COUNT) {
				if (i + tidy < height) {
					int a = sResult[i][laneId + DST_OFFSET];
					dst[index] = sResult[i][laneId + DST_OFFSET];
					index += widthStride*WARP_COUNT;
				}
			}
		}
#endif
	}

#endif

#if 1
	template<>
	__global__ void kerenlOneBlockOneChannel<float, 8, 1, 3, 3, 3, 64, 96>(int BLOCK_NUM_X, const float* __restrict__ src, float* dst, int width, int widthStride, int height, int channels,
		const float* __restrict__ filter, int fx, int fy, int fc, int fn, int padw, int padh)
	{
		typedef float T;
		const int PROCESS_DATA_COUNT = 8;
		const int PROCESS_FILTER_COUNT = 1;
		const int FILTER_WIDTH = 3;
		const int FILTER_HEIGHT = 3;
		const int FILTER_CHANNELS = 3;
		const int FILTER_NUMBER = 64;
		const int BLOCK_DIM_X = 96;

		const int MAX_FILTER_COUNT = FILTER_NUMBER;
		//const int REG_COUNT = PROCESS_DATA_COUNT*MAX_FILTER_COUNT + FILTER_HEIGHT;
		const int WARP_COUNT = BLOCK_DIM_X >> 5;
		const int BLOCK_DATA_X = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_DATA_Y = PROCESS_DATA_COUNT;
		const int DST_OFFSET = FILTER_WIDTH - 1;
		const int GRID_DIM_X = gridDim.x;
		const int GRID_DIM_Y = gridDim.y;
		assert(BLOCK_DIM_X == blockDim.x);

		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;

		const int BLOCK_IDX_X = blockIdx.x % BLOCK_NUM_X;
		const int BLOCK_IDX_Y = blockIdx.x / BLOCK_NUM_X;

		int tidx = laneId + BLOCK_DATA_X*BLOCK_IDX_X;
		int tidy = threadIdx.y + BLOCK_DATA_Y*blockIdx.y;

		__shared__ T smem[PROCESS_FILTER_COUNT][FILTER_CHANNELS][FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0][0][0];

		const int filter_size = FILTER_WIDTH*FILTER_HEIGHT * 3;
		const int offset = BLOCK_IDX_Y*filter_size;

		if (threadIdx.x < filter_size) {
			psmem[threadIdx.x] = filter[threadIdx.x + offset];
		}
		__syncthreads();

		//T data[FILTER_HEIGHT + PROCESS_DATA_COUNT - 1];
		//T result[PROCESS_DATA_COUNT];

		T data0, data1, data2, data3, data4, data5, data6, data7, data8, data9;
		T result0, result1, result2, result3, result4, result5, result6, result7;

		int index = tidx + widthStride*tidy + warpId*widthStride*height;

		{ const int s = 0; if (tidy + s < height && tidx < width)  data0 = src[index]; else data0 = 0; index += widthStride; }
		{ const int s = 1; if (tidy + s < height && tidx < width)  data1 = src[index]; else data1 = 0; index += widthStride; }
		{ const int s = 2; if (tidy + s < height && tidx < width)  data2 = src[index]; else data2 = 0; index += widthStride; }
		{ const int s = 3; if (tidy + s < height && tidx < width)  data3 = src[index]; else data3 = 0; index += widthStride; }
		{ const int s = 4; if (tidy + s < height && tidx < width)  data4 = src[index]; else data4 = 0; index += widthStride; }
		{ const int s = 5; if (tidy + s < height && tidx < width)  data5 = src[index]; else data5 = 0; index += widthStride; }
		{ const int s = 6; if (tidy + s < height && tidx < width)  data6 = src[index]; else data6 = 0; index += widthStride; }
		{ const int s = 7; if (tidy + s < height && tidx < width)  data7 = src[index]; else data7 = 0; index += widthStride; }
		{ const int s = 8; if (tidy + s < height && tidx < width)  data8 = src[index]; else data8 = 0; index += widthStride; }
		{ const int s = 9; if (tidy + s < height && tidx < width)  data9 = src[index]; else data9 = 0; index += widthStride; }

		{
			const int s = 0;
			{
				const int i = 0;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data0 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data2 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data0 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data2 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data0 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data2 * smem[s][warpId][n][m]; }
				}
				result0 = sum;
			}
			{
				const int i = 1;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data3 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data3 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data1 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data3 * smem[s][warpId][n][m]; }
				}
				result1 = sum;
			}
			{
				const int i = 2;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data4 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data4 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data2 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data4 * smem[s][warpId][n][m]; }
				}
				result2 = sum;
			}
			{
				const int i = 3;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data5 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data5 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data3 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data5 * smem[s][warpId][n][m]; }
				}
				result3 = sum;
			}
			{
				const int i = 4;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data6 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data6 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data4 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data6 * smem[s][warpId][n][m]; }
				}
				result6 = sum;
			}
			{
				const int i = 5;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data7 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data7 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data5 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data7 * smem[s][warpId][n][m]; }
				}
				result5 = sum;
			}
			{
				const int i = 6;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data8 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data8 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data6 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data8 * smem[s][warpId][n][m]; }
				}
				result6 = sum;
			}
			{
				const int i = 7;
				T sum = 0;
				{
					const int m = 0;
					{ const int n = 0; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data8 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data9 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 1; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data8 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data9 * smem[s][warpId][n][m]; }
				}
				{
					const int m = 2; sum = __shfl_up(sum, 1);
					{ const int n = 0; sum += data7 * smem[s][warpId][n][m]; }
					{ const int n = 1; sum += data8 * smem[s][warpId][n][m]; }
					{ const int n = 2; sum += data9 * smem[s][warpId][n][m]; }
				}
				result7 = sum;
			}
		}
		__shared__ T sResult[PROCESS_DATA_COUNT][WARP_SIZE];

		assert(WARP_COUNT == 3);
		if (warpId == 0)
		{
			{ const int i = 0; sResult[i][laneId] = result0; }
			{ const int i = 1; sResult[i][laneId] = result1; }
			{ const int i = 2; sResult[i][laneId] = result2; }
			{ const int i = 3; sResult[i][laneId] = result3; }
			{ const int i = 4; sResult[i][laneId] = result4; }
			{ const int i = 5; sResult[i][laneId] = result5; }
			{ const int i = 6; sResult[i][laneId] = result6; }
			{ const int i = 7; sResult[i][laneId] = result7; }
		}
		__syncthreads();
		if (warpId == 1)
		{
			{ const int i = 0; sResult[i][laneId] += result0; }
			{ const int i = 1; sResult[i][laneId] += result1; }
			{ const int i = 2; sResult[i][laneId] += result2; }
			{ const int i = 3; sResult[i][laneId] += result3; }
			{ const int i = 4; sResult[i][laneId] += result4; }
			{ const int i = 5; sResult[i][laneId] += result5; }
			{ const int i = 6; sResult[i][laneId] += result6; }
			{ const int i = 7; sResult[i][laneId] += result7; }
		}
		__syncthreads();
		if (warpId == 2)
		{
			{ const int i = 0; sResult[i][laneId] += result0; }
			{ const int i = 1; sResult[i][laneId] += result1; }
			{ const int i = 2; sResult[i][laneId] += result2; }
			{ const int i = 3; sResult[i][laneId] += result3; }
			{ const int i = 4; sResult[i][laneId] += result4; }
			{ const int i = 5; sResult[i][laneId] += result5; }
			{ const int i = 6; sResult[i][laneId] += result6; }
			{ const int i = 7; sResult[i][laneId] += result7; }
		}
		__syncthreads();
#if 1
		//if (BLOCK_IDX_Y > 0 || tidy > 0) return;

		if (laneId < WARP_SIZE - DST_OFFSET && tidx < width/* && tidy < height */)
		{
			index = tidx + widthStride*(tidy + warpId) + BLOCK_IDX_Y*widthStride*height;
			//#pragma unroll
			for (int i = warpId; i < PROCESS_DATA_COUNT; i += WARP_COUNT) {
				if (i + tidy < height) {
					int a = sResult[i][laneId + DST_OFFSET];
					dst[index] = sResult[i][laneId + DST_OFFSET];
					index += widthStride*WARP_COUNT;
				}
			}
		}
#endif
	}

#endif

	static void TestOneBlockOneChannel() {
		typedef float DataType;
		const int width = 514;
		const int height = 514;
		const int channels = 3;

		const int filter_width = 3;
		const int filter_height = 3;
		const int filter_channels = 3;
		const int filter_number = 32;

		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> src, filter, dst;
		char szPath[1024] = "";
		src.MallocBuffer(width, height, channels, 1);
		filter.MallocBuffer(filter_width, filter_height, channels, filter_number);
		dst.MallocBuffer(width, height, filter.count, 1);

		for (int i = 0; i < src.Size(); i++) {
			//src.data[i] = i + 1;
			src.data[i] = 1;
		}
		for (int i = 0; i < filter.Size(); i++) {
			//filter.data[i] = i + 1;
			filter.data[i] = 1;
		}

		//sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		//bool bRtn = img.Load<uchar>(szPath, width, height);
		//if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(src.width, src.height, src.channels), devDst(dst.width, dst.height, dst.channels), devFilter(filter.Size());
		devSrc.CopyFromHost(src.data, src.width, src.width, src.height, src.channels);
		devFilter.CopyFromHost(filter.data, filter.Size(), filter.Size(), 1);

		const int PROCESS_DATA_COUNT = 8;
		const int PROCESS_FILTER_COUNT = 1;
		const int FILTER_WIDTH = filter_width;
		const int FILTER_HEIGHT = filter_height;
		const int BLOCK_DIM_X = WARP_SIZE*channels;
		const int BLOCK_NUM_X = filter.count;

		dim3 block_size(BLOCK_DIM_X, 1);

		const int BLOCK_DATA_X = (BLOCK_DIM_X / WARP_SIZE * (WARP_SIZE - 2));

		dim3 grid_size(UpDivide(width - FILTER_WIDTH / 2 * 2, WARP_SIZE - FILTER_WIDTH / 2 * 2)*BLOCK_NUM_X, UpDivide(height, PROCESS_DATA_COUNT)/*-FILTER_HEIGHT/2*2*/);
		printf("%d %d, %d %d\n", grid_size.x, grid_size.y, block_size.x, block_size.y);
		cudaEventRecord(start, 0);
		kerenlOneBlockOneChannel<DataType, PROCESS_DATA_COUNT, PROCESS_FILTER_COUNT, FILTER_WIDTH, FILTER_HEIGHT, filter_channels, filter_number, BLOCK_DIM_X> << <grid_size, block_size >> >
			(grid_size.x / BLOCK_NUM_X, devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, channels, devFilter.GetData(), filter.width, filter.height, filter.channels, filter.count, filter.width / 2, filter.height / 2);
		cudaDeviceSynchronize();
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(dst.data, dst.width, dst.width, dst.height, dst.channels);
		CUDA_CHECK_ERROR;

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		printf("%dx%dx%d , %dx%dx%dx%d, %f ms , %f fps\n", width, height, channels, filter_width, filter_height, filter_channels, filter_number, inc, 1000.0 / inc);

		sprintf(szPath, "../data/data%dx%dx%d.txt", width, dst.width, dst.height, dst.channels);
		dst.SaveText(szPath);
	}
};

void Test_PipConv2_1() {
	Conv2_1::TestOneBlockOneChannel();
}