#include "../conv/common.h"
#include "../conv/cudaLib.cuh"

namespace CONV2D_Sim {
	static const int WARP_SIZE = 32;

	template<typename T, int BLOCK_SIZE, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT>
	__global__ void kernel_convolution2D(
		const T* __restrict__ src, T* dst, int width, int widthStride, int height,
		const T* __restrict__ weight) {
#if 1
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

		T data[DATA_CACHE_SIZE];

		const int process_count = BLOCK_PROCESS_DATA_COUNT*blockIdx.x + WARP_PROCESS_DATA_COUNT*warpId;
		if (process_count >= width)
			return;
		int tidx = process_count + laneId - FILTER_WIDTH / 2;
		int tidy = PROCESS_DATA_COUNT*blockIdx.y - FILTER_HEIGHT / 2;


		__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0];
		for (int i=threadIdx.x; i < FILTER_HEIGHT*FILTER_WIDTH; i += blockDim.x)
				psmem[i] = weight[i];
		__syncthreads();

		int index = widthStride*tidy + tidx;
#pragma unroll
		for (int s = 0; s < DATA_CACHE_SIZE; s++) {
			int _tidy = tidy + s;
			if (tidx >= 0 && tidx < width && _tidy >= 0 && _tidy < height) {
				data[s] = src[index];
			}
			else {
				data[s] = 0;
			}
			index += widthStride;
		}
//		T* p = &data[0];
#pragma unroll
		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
			T sum = 0;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0) {
					sum = __my_shfl_up(sum, 1);
				}
				//int a = data[i + 0];
				//int b = data[i + 1];
				//int c = data[i + 2];
				//int d = data[i + 3];
				//int e = data[i + 4];
				//int f = data[i + 5];
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
//					int a = data[i + 0];

					//sum += data[i + n] * smem[n][m];
					sum = MAD(data[i + n], smem[n][m], sum);
				}
			}
			data[i] = sum;
		}

		index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1)/2;
#pragma unroll
		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
			if (laneId >= FILTER_WIDTH - 1 && tidx - (FILTER_WIDTH - 1) / 2 < width && tidy + FILTER_HEIGHT / 2 + i < height) {
				dst[index] = data[i];
			}
			index += widthStride;
		}
		/**/
#endif
	}

	template<int FILTER_WIDTH, int FILTER_HEIGHT, int PROCESS_DATA_COUNT, int BLOCK_SIZE>
	static float Test_conv2D(int width, int height) {
		typedef float DataType;

		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;

		const int nRepeatCount = 3;
		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> img;
		char szPath[1024] = "";
		sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		bool bRtn = false;
		bRtn = img.Load<uchar>(szPath, width, height);
		//sprintf(szPath, "../data/Lena%dx%d.txt", width, height);
		//img.SaveText(szPath);
		if (!bRtn) img.MallocBuffer(width, height);
		//for (int i = 0; i < img.width*img.height; i++) img.data[i] = 1;
		if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(width, height), devDst(width, height);
		devSrc.CopyFromHost(img.data, img.width, img.width, img.height);
		DataT<DataType> imgDst;
		imgDst.MallocBuffer(width, height);

		dim3 block_size(BLOCK_SIZE, 1);
		dim3 grid_size(UpDivide(width, BLOCK_PROCESS_DATA_COUNT), UpDivide(height, PROCESS_DATA_COUNT));

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ -1.0,  -1.0, -1.0, },
			{ -1.0, 8.0, -1.0, },
			{ -1.0,  -1.0, -1.0, },
		};
#endif

#if 1
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  0.0, },
			{ 0,  -1.0, },
//			{ -1.0,  -2.0, -1.0, },
		};
#endif

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  1.0, 1.0, },
			{ 1.0, 1.0, 1.0, },
			{ 1.0,  1.0, 1.0, },
		};
#endif
		DataType* flt = &filter[0][0];
		for (int i = 0; i < FILTER_HEIGHT*FILTER_WIDTH; i++)
			flt[i] = -1;
		flt[FILTER_HEIGHT*FILTER_WIDTH / 2] = FILTER_HEIGHT*FILTER_WIDTH - 1;

		DevData<DataType> devFilter(FILTER_HEIGHT*FILTER_WIDTH);
		devFilter.CopyFromHost(&filter[0][0], FILTER_WIDTH*FILTER_HEIGHT, FILTER_WIDTH*FILTER_HEIGHT, 1);

		cudaEventRecord(start, 0);
		//watch.start();
		//Conv2D(devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData(), FILTER_WIDTH, FILTER_HEIGHT);
		for (int s = 0; s < nRepeatCount; s++) {
			kernel_convolution2D<DataType, BLOCK_SIZE, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT> << <grid_size, block_size >> > (devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData());
			cudaDeviceSynchronize();
		}
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(imgDst.data, imgDst.width, imgDst.width, imgDst.height);

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		inc /= (float)nRepeatCount;
		printf("%dx%d , %dx%d , proc_count=%d, cache=%d, BLOCK_SIZE=%d, %f ms , %f fps\n", width, height, FILTER_WIDTH, FILTER_HEIGHT, PROCESS_DATA_COUNT, PROCESS_DATA_COUNT+FILTER_HEIGHT-1, BLOCK_SIZE, inc, 1000.0 / inc);
		sprintf(szPath, "../data/Lena_proc_%dx%d.raw", width, height);
		//imgDst.SaveRaw(szPath);

		sprintf(szPath, "../data/Lena_proc_%dx%d.txt", width, height);
		//imgDst.SaveText(szPath);

		DataT<DataType> imgVerify;
		imgVerify.MallocBuffer(width, height);
		Convolution(img.data, imgVerify.data, width, height, filter[0], FILTER_WIDTH, FILTER_HEIGHT);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);
		
		double dif = 0;
		for (int i = 0; i < img.width*img.height; i++) {
			dif += abs(imgVerify.data[i] - imgDst.data[i]);
		}	
		printf("verify dif =%f\n", dif);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);
		sprintf(szPath, "../data/Lena_proc_verify(%dx%d)_%dx%d.raw", FILTER_WIDTH, FILTER_HEIGHT, width, height);
		//imgVerify.SaveRaw(szPath);

		FILE* fp = fopen("log.conv2D.csv", "at");
		if (fp) {
			fprintf(fp, "%dx%d, %d_%d, %d, %dx%d, %f\n", width, height, PROCESS_DATA_COUNT, PROCESS_DATA_COUNT + FILTER_HEIGHT - 1, BLOCK_SIZE, FILTER_WIDTH, FILTER_HEIGHT, inc);
			fclose(fp);
		}
		return inc;
	}
};

#if 0
template<int BLOCK_SIZE, int FilterSize>
inline void Diff2D_Cache(int width, int height) {
#if 0
	{const int CacheC = FilterSize; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 1; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 2; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 3; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 4; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 5; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 6; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 7; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 8; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 9; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 10; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 11; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 12; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 13; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 14; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 15; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 16; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 17; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 18; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 19; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 20; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 21; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 22; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 23; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 24; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 25; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 26; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 27; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 28; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 29; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int CacheC = FilterSize + 30; const int PROCESS_DATA_COUNT = CacheC - FilterSize + 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
#else
	{const int PROCESS_DATA_COUNT = 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 2; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 3; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 4; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 5; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 6; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 7; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 8; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 9; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 10; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 11; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 12; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 13; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 14; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 15; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 16; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 17; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 18; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 19; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 20; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 21; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 22; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 23; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 24; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 25; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 26; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 27; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 28; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 29; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{const int PROCESS_DATA_COUNT = 30; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }


#endif
}
static void Test_2D_Diff_Cache(int argc, char** argv) {
	int width = 8192;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;
	const int BLOCK_SIZE = 256;
	{
		const int FILTER_SIZE = 3;
		Diff2D_Cache<BLOCK_SIZE, FILTER_SIZE>(width, height);
	}
	{
		const int FILTER_SIZE = 5;
		Diff2D_Cache<BLOCK_SIZE, FILTER_SIZE>(width, height);
	}
	{
		const int FILTER_SIZE = 7;
		Diff2D_Cache<BLOCK_SIZE, FILTER_SIZE>(width, height);
	}
	{
		const int FILTER_SIZE = 9;
		Diff2D_Cache<BLOCK_SIZE, FILTER_SIZE>(width, height);
	}
	{
		const int FILTER_SIZE = 11;
		Diff2D_Cache<BLOCK_SIZE, FILTER_SIZE>(width, height);
	}
}
#endif
//------------------------------------------------------------------
#if 0
template<int BLOCK_SIZE>
static void TestDiffBlocksize(int width, int height) {
	const int PROCESS_DATA_COUNT = 8;
	{
		//const int BLOCK_SIZE = 32 * 1;
		//for (int i = 0; i<100; i++)
		{ const int FilterSize = 2;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 3;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 4;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 5;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 6;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 7;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 8;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 9;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 10;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 11;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 12;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 13;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 14;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 15;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 16;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 17;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 18;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 19;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 20;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 21;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 22;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 23;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 24;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 25;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 26;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 27;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 28;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 29;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 30;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 31;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		{ const int FilterSize = 32;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	}
}

static void TestDiffBlocksize(int argc, char** argv) {
	int width = 1024;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;
	TestDiffBlocksize<32 * 1>(width, height);
	//TestDiffBlocksize<32 * 2>(width, height);
	//TestDiffBlocksize<32 * 3>(width, height);
	//TestDiffBlocksize<32 * 4>(width, height);
	//TestDiffBlocksize<32 * 5>(width, height);
	//TestDiffBlocksize<32 * 6>(width, height);
	//TestDiffBlocksize<32 * 7>(width, height);
	//TestDiffBlocksize<32 * 8>(width, height);
	//TestDiffBlocksize<32 * 9>(width, height);
	//TestDiffBlocksize<32 * 11>(width, height);
	//TestDiffBlocksize<32 * 12>(width, height);
	//TestDiffBlocksize<32 * 13>(width, height);
	//TestDiffBlocksize<32 * 14>(width, height);
	//TestDiffBlocksize<32 * 15>(width, height);
	//TestDiffBlocksize<32 * 16>(width, height);

}
#endif
//------------------------------------------------------------------
//Access Global Memory directly
void Test_2D_simple(int argc, char** argv) {
	DISPLAY_FUNCTION("evaluate");

	//Test_2D_Diff_Cache(argc, argv);
#if 0
	TestDiffBlocksize(argc, argv);
#endif

#if 0 // _DEBUG
	const int PROCESS_DATA_COUNT = 5; 
	int width = 9216;
	int height = width;	

	CONV2D_Sim::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	//CONV2D_Sim::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	//CONV2D_Sim::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	return;

	Test_2D_Diff_Cache();
	return;
	//for (int i = 0; i<100; i++)
	CONV2D_Sim::Test_conv2D<2, 2, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<4, 4, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<6, 6, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<8, 8, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<9, 9, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<10, 10, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<11, 11, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<12, 12, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<13, 13, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<14, 14, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<15, 15, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<16, 16, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<17, 17, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<18, 18, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<19, 19, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<20, 20, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<21, 21, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<22, 22, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<23, 23, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<24, 24, PROCESS_DATA_COUNT>(width, height);
/*	CONV2D_Sim::Test_conv2D<25, 25, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<26, 26, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<27, 27, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<28, 28, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<29, 29, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<30, 30, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<31, 31, PROCESS_DATA_COUNT>(width, height);
	CONV2D_Sim::Test_conv2D<32, 32, PROCESS_DATA_COUNT>(width, height);*/
	
#endif

#if 1
	int width = 1024;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;
	const int PROCESS_DATA_COUNT = 11;
	{
		{
			const int BLOCK_SIZE = 128;
			//for (int i = 0; i<100; i++)
			{ const int FilterSize = 2;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 3;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 4;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 5;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 6;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 7;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 8;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 9;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 10;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		}
		{
			const int BLOCK_SIZE = 64;
			{ const int FilterSize = 11;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 12;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 13;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 14;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 15;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 16;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 17;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 18;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 19;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 20;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		}
		{
			const int BLOCK_SIZE = 32;
			{ const int FilterSize = 21;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 22;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 23;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 24;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 25;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 26;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 27;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 28;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 29;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 30;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 31;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
			{ const int FilterSize = 32;  CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
		}
	}
#endif

#if 0
	int width = 8192;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;
	
	const int BLOCK_SIZE = 512;
	//for (int i = 0; i<100; i++)
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 1; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 2; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 3; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 4; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 5; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 6; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 7; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 8; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 9; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 10; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 11; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 12; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 13; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 14; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 15; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 16; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 17; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 18; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 19; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 20; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 21; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 32; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 23; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 24; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 25; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 26; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 27; CONV2D_Sim::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }

#endif
}




