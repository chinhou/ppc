#include "../conv/common.h"
#include "../conv/cudaLib.cuh"

namespace CONV_01_01 {
	static const int WARP_SIZE = 32;

	template<typename T, int BLOCK_SIZE, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT>
	__global__ void kernel_convolution2D(
		const T* __restrict__ src, T* dst, int width, int widthStride, int height,
		const T* __restrict__ weight) {
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

		T data[DATA_CACHE_SIZE];

		int tidx = BLOCK_PROCESS_DATA_COUNT*blockIdx.x + WARP_PROCESS_DATA_COUNT*warpId + laneId - FILTER_WIDTH / 2;
		int tidy = PROCESS_DATA_COUNT*blockIdx.y - FILTER_HEIGHT / 2;

		__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0];
		if (threadIdx.x < FILTER_HEIGHT*FILTER_WIDTH)
			psmem[threadIdx.x] = weight[threadIdx.x];
		__syncthreads();

		int index = widthStride*tidy + tidx;
#pragma unroll
		for (int s = 0; s < DATA_CACHE_SIZE; s++) {
			int _tidy = tidy + s;
			if (tidx >= 0 && tidx < width && _tidy >= 0 && _tidy < height) {
				data[s] = src[index];
			}
			else {
				data[s] = 0;
			}
			index += widthStride;
		}
		T* p = &data[0];
#pragma unroll
		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
			T sum = 0;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0)
					sum = __shfl_up(sum, 1);
				int a = data[i + 0];
				int b = data[i + 1];
				int c = data[i + 2];
				int d = data[i + 3];
				int e = data[i + 4];
				int f = data[i + 5];
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
					int a = data[i + 0];

					sum += data[i + n] * smem[n][m];
					//sum = Mad(data[i + n], smem[n][m], sum);
				}
			}
			data[i] = sum;
		}

		index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - 1;
		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
			if (laneId >= FILTER_WIDTH - 1 && tidx - 1 < width && tidy + FILTER_HEIGHT / 2 + i < height) {
				dst[index] = data[i];
			}
			index += widthStride;
		}
		/**/
	}

	static float Test_conv2D() {
		typedef float DataType;

		int width = 9216; // 1024 * 8;
		int height = width;
		const int FILTER_WIDTH = 3;
		const int FILTER_HEIGHT = 3;
		const int BLOCK_SIZE = 1024;
		const int PROCESS_DATA_COUNT = 5;

		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;


		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> img;
		char szPath[1024] = "";
		sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		bool bRtn = img.Load<uchar>(szPath, width, height);
		//if (!bRtn) img.MallocBuffer(width, height);
		//for (int i = 0; i < img.width*img.height; i++) img.data[i] = 1;
		if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(width, height), devDst(width, height);
		devSrc.CopyFromHost(img.data, img.width, img.width, img.height);
		DataT<DataType> imgDst;
		imgDst.MallocBuffer(width, height);

		dim3 block_size(BLOCK_SIZE, 1);
		dim3 grid_size(UpDivide(width, BLOCK_PROCESS_DATA_COUNT), UpDivide(height, PROCESS_DATA_COUNT));

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ -1.0,  -1.0, -1.0,},
			{ -1.0, 8.0, -1.0,},
			{ -1.0,  -1.0, -1.0,},
		};
#endif

#if 1
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  2.0, 1.0, },
			{ 0,  0.0, 0, },
			{ -1.0,  -2.0, -1.0, },
		};
#endif

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  1.0, 1.0, },
			{ 1.0, 1.0, 1.0, },
			{ 1.0,  1.0, 1.0, },
		};
#endif
		DevData<DataType> devFilter(FILTER_WIDTH*FILTER_WIDTH);
		devFilter.CopyFromHost(&filter[0][0], FILTER_WIDTH*FILTER_HEIGHT, FILTER_WIDTH*FILTER_HEIGHT, 1);

		cudaEventRecord(start, 0);
		//watch.start();

		kernel_convolution2D<DataType, BLOCK_SIZE, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT> << <grid_size, block_size >> > (devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData());
		cudaDeviceSynchronize();
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(imgDst.data, imgDst.width, imgDst.width, imgDst.height);

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		printf("%dx%d , %f ms , %f fps\n", width, height, inc, 1000.0 / inc);
		sprintf(szPath, "../data/Lena_proc_%dx%d.raw", width, height);
		//imgDst.SaveRaw(szPath);
		//sprintf(szPath, "../data/Lena_proc_%dx%d.txt", width, height);
		//imgDst.SaveText(szPath);
		return inc;
	}
};

//Access Global Memory directly
void Test_01_01() {
	DISPLAY_FUNCTION("benchmark");
	//for (int i = 0; i<100; i++)
	CONV_01_01::Test_conv2D();
}














