#include "../conv/common.h"
#include "../conv/cudaLib.cuh"

namespace CONV2D_GlobalMem {
	static const int WARP_SIZE = 32;

	template<typename T, int BLOCK_SIZE, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT>
	__global__ void kernel_convolution2D(
		const T* __restrict__ src, T* dst, int width, int widthStride, int height,
		const T* __restrict__ weight) {
#if 1
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

		//T data[DATA_CACHE_SIZE];
		//__shared__ T sdata[BLOCK_SIZE*DATA_CACHE_SIZE];

		int tidx = BLOCK_PROCESS_DATA_COUNT*blockIdx.x + WARP_PROCESS_DATA_COUNT*warpId + laneId - FILTER_WIDTH / 2;
		int tidy = PROCESS_DATA_COUNT*blockIdx.y - FILTER_HEIGHT / 2;

		__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0];
		if (threadIdx.x < FILTER_HEIGHT*FILTER_WIDTH)
			psmem[threadIdx.x] = weight[threadIdx.x];

		int index = widthStride*tidy + tidx;
//		int sMemIdx = threadIdx.x;
//#pragma unroll
//		for (int s = 0; s < DATA_CACHE_SIZE; s++) {
//			int _tidy = tidy + s;
//			if (tidx >= 0 && tidx < width && _tidy >= 0 && _tidy < height) {
//				//data[s] = src[index];
//				sdata[sMemIdx] = src[index];
//			}
//			else {
//				//data[s] = 0;
//				sdata[sMemIdx] = 0;
//			}
//			index += widthStride;
//			sMemIdx += BLOCK_SIZE;
//		}
		__syncthreads();

		int dst_index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1) / 2;
		//T* p = &data[0];
#pragma unroll
		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
			T sum = 0;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0) {
					sum = __my_shfl_up(sum, 1);
				}
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
					//int a = data[i + 0];

					//sum += data[i + n] * smem[n][m];
					//sum = MAD(data[i + n], smem[n][m], sum);
					//sum = MAD(data[i + n], smem[n][m], sum);
					//int smemIdx = (i + n)*BLOCK_SIZE + threadIdx.x;
					//sum = MAD(sdata[smemIdx], smem[n][m], sum);

					T val;
					const int s = i + n;
					int _tidy = tidy + s;
					if (tidx >= 0 && tidx < width && _tidy >= 0 && _tidy < height) {
						val = src[index + s*widthStride];
					}else {
						val = 0;
					}
					sum = MAD(val, smem[n][m], sum);
				}
			}
			//data[i] = sum;
			if (laneId >= FILTER_WIDTH - 1 && tidx - (FILTER_WIDTH - 1) / 2 < width && tidy + FILTER_HEIGHT / 2 + i < height) {
				dst[dst_index] = sum;
			}
			dst_index += widthStride;
		}

//		int dst_index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1)/2;
//#pragma unroll
//		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
//			if (laneId >= FILTER_WIDTH - 1 && tidx - (FILTER_WIDTH - 1) / 2 < width && tidy + FILTER_HEIGHT / 2 + i < height) {
//				dst[dst_index] = data[i];
//			}
//			dst_index += widthStride;
//		}
//		/**/
#endif
	}

	template<typename T, int BLOCK_SIZE, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT> inline
	void Convolution2D(const T* src, T* dst, int width, int widthPitch, int height, T* filter, int filter_width, int filter_height) {
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		dim3 block_size(BLOCK_SIZE, 1);
		dim3 grid_size(UpDivide(width, BLOCK_PROCESS_DATA_COUNT), UpDivide(height, PROCESS_DATA_COUNT));
#define MyKernel kernel_convolution2D<T, BLOCK_SIZE, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT>
		cudaFuncSetCacheConfig(MyKernel, cudaFuncCachePreferL1);
		MyKernel << <grid_size, block_size >> > (src, dst, width, widthPitch, height, filter);
#undef MyKernel
	}

	template<typename T, int FILTER_WIDTH, int FILTER_HEIGHT> inline
	void _Conv2D(const T* src, T* dst, int width, int widthPitch, int height, T* filter) {
		static const int cBlockSize[] = {
			1024, 1024, 1024, 1024, 1024, 960, 960, 960, 960, 960, 960, 960, 960, 960, 960,
		};
		assert(FILTER_WIDTH == FILTER_HEIGHT);
		const int BLOCK_SIZE = FILTER_WIDTH <= 5?1024:512;
		const int PROCESS_DATA_COUNT = 8;
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		Convolution2D<T, BLOCK_SIZE, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT>(src, dst, width, widthPitch, height, filter, FILTER_WIDTH, FILTER_HEIGHT);
	}

	template<typename T> inline
	void Conv2D(const T* src, T* dst, int width, int widthPitch, int height, T* filter, int filter_width, int filter_height) {
		switch (filter_width)
		{
#define CASE_CONV(_x) case _x: _Conv2D<T, _x, _x>(src, dst, width, widthPitch, height, filter); break;	
		CASE_CONV(2);
		CASE_CONV(3);
		CASE_CONV(4);
		CASE_CONV(5);
		CASE_CONV(6);
		CASE_CONV(7);
		CASE_CONV(8);
		CASE_CONV(9);
		CASE_CONV(10);
		CASE_CONV(11);
		CASE_CONV(12);
		CASE_CONV(13);
		CASE_CONV(14);
		CASE_CONV(15);
		CASE_CONV(16);
		CASE_CONV(17);
		CASE_CONV(18);
		CASE_CONV(19);	
		CASE_CONV(20);
		CASE_CONV(21);	
		CASE_CONV(22);
		CASE_CONV(23);
		CASE_CONV(24);
		CASE_CONV(25);
		CASE_CONV(26);
		CASE_CONV(27);
		CASE_CONV(28);
		CASE_CONV(29);		
		CASE_CONV(30);
		CASE_CONV(31);
		CASE_CONV(32);		
		default:
			break;
		}

	}

	template<int FILTER_WIDTH, int FILTER_HEIGHT, int PROCESS_DATA_COUNT, int BLOCK_SIZE>
	static float Test_conv2D(int width, int height) {
		typedef float DataType;

		//int width =  9216; // 1024 * 8;
		//int height = width;
		//const int FILTER_WIDTH = 3;
		//const int FILTER_HEIGHT = FILTER_WIDTH;
		//const int BLOCK_SIZE = 512;
		//const int PROCESS_DATA_COUNT = 1;

		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;


		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> img;
		char szPath[1024] = "";
		sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		bool bRtn = img.Load<uchar>(szPath, width, height);
		sprintf(szPath, "../data/Lena%dx%d.txt", width, height);
		//img.SaveText(szPath);
		//if (!bRtn) img.MallocBuffer(width, height);
		//for (int i = 0; i < img.width*img.height; i++) img.data[i] = 1;
		if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(width, height), devDst(width, height);
		devSrc.CopyFromHost(img.data, img.width, img.width, img.height);
		DataT<DataType> imgDst;
		imgDst.MallocBuffer(width, height);

		dim3 block_size(BLOCK_SIZE, 1);
		dim3 grid_size(UpDivide(width, BLOCK_PROCESS_DATA_COUNT), UpDivide(height, PROCESS_DATA_COUNT));

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ -1.0,  -1.0, -1.0, },
			{ -1.0, 8.0, -1.0, },
			{ -1.0,  -1.0, -1.0, },
		};
#endif

#if 1
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  0.0, },
			{ 0,  -1.0, },
//			{ -1.0,  -2.0, -1.0, },
		};
#endif

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  1.0, 1.0, },
			{ 1.0, 1.0, 1.0, },
			{ 1.0,  1.0, 1.0, },
		};
#endif
		DataType* flt = &filter[0][0];
		//filter[0][0] = 1;
		//filter[FILTER_HEIGHT-1][FILTER_WIDTH-1] = -1;
		for (int i = 0; i < FILTER_HEIGHT / 2; i++) {
			for (int j = 0; j < FILTER_WIDTH; j++) {
				filter[FILTER_HEIGHT - 1 - i][FILTER_WIDTH - 1 - j] = -1;
				filter[i][j] = -1;
			}
		}

		DevData<DataType> devFilter(FILTER_HEIGHT*FILTER_WIDTH);
		devFilter.CopyFromHost(&filter[0][0], FILTER_WIDTH*FILTER_HEIGHT, FILTER_WIDTH*FILTER_HEIGHT, 1);

		cudaEventRecord(start, 0);
		//watch.start();
		//Conv2D(devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData(), FILTER_WIDTH, FILTER_HEIGHT);
		kernel_convolution2D<DataType, BLOCK_SIZE, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT> << <grid_size, block_size >> > (devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData());
		cudaDeviceSynchronize();
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(imgDst.data, imgDst.width, imgDst.width, imgDst.height);

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		printf("%dx%d , %dx%d , proc_count=%d, BLOCK_SIZE=%d, %f ms , %f fps\n", width, height, FILTER_WIDTH, FILTER_HEIGHT, PROCESS_DATA_COUNT, BLOCK_SIZE, inc, 1000.0 / inc);
		sprintf(szPath, "../data/Lena_proc_%dx%d.raw", width, height);
		//imgDst.SaveRaw(szPath);

		sprintf(szPath, "../data/Lena_proc_%dx%d.txt", width, height);
		//imgDst.SaveText(szPath);

		DataT<DataType> imgVerify;
		imgVerify.MallocBuffer(width, height);
		Convolution(img.data, imgVerify.data, width, height, filter[0], FILTER_WIDTH, FILTER_HEIGHT);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);
		
		double dif = 0;
		for (int i = 0; i < img.width*img.height; i++) {
			dif += abs(imgVerify.data[i] - imgDst.data[i]);
		}	
		printf("verify dif =%f\n", dif);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.raw", width, height);
		//imgVerify.SaveRaw(szPath);

		return inc;
	}
};


//Access Global Memory directly
void Test_2D_GlobalMem() {
	DISPLAY_FUNCTION("evaluate");


#if 0 // _DEBUG
	const int PROCESS_DATA_COUNT = 5; 
	int width = 9216;
	int height = width;	

	CONV2D_GlobalMem::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	//CONV2D_GlobalMem::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	//CONV2D_GlobalMem::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	return;

	Test_2D_Diff_Cache();
	return;
	//for (int i = 0; i<100; i++)
	CONV2D_GlobalMem::Test_conv2D<2, 2, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<4, 4, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<6, 6, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<8, 8, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<9, 9, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<10, 10, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<11, 11, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<12, 12, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<13, 13, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<14, 14, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<15, 15, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<16, 16, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<17, 17, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<18, 18, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<19, 19, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<20, 20, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<21, 21, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<22, 22, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<23, 23, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<24, 24, PROCESS_DATA_COUNT>(width, height);
/*	CONV2D_GlobalMem::Test_conv2D<25, 25, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<26, 26, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<27, 27, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<28, 28, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<29, 29, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<30, 30, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<31, 31, PROCESS_DATA_COUNT>(width, height);
	CONV2D_GlobalMem::Test_conv2D<32, 32, PROCESS_DATA_COUNT>(width, height);*/
	
#else
	int width = 9216;
	int height = width;
	const int PROCESS_DATA_COUNT = 8;
	const int BLOCK_SIZE = 512;
	//for (int i = 0; i<100; i++)
	CONV2D_GlobalMem::Test_conv2D<3, 3, PROCESS_DATA_COUNT, 1024>(width, height);
	CONV2D_GlobalMem::Test_conv2D<5, 5, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height);
	CONV2D_GlobalMem::Test_conv2D<7, 7, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height);
	CONV2D_GlobalMem::Test_conv2D<9, 9, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height);
	CONV2D_GlobalMem::Test_conv2D<11, 11, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height);
	CONV2D_GlobalMem::Test_conv2D<13, 13, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height);
	CONV2D_GlobalMem::Test_conv2D<15, 15, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height);
#endif
}














