#include "../conv/common.h"
#include "../conv/cudaLib.cuh"

namespace CONV2D_multi {
	static const int WARP_SIZE = 32;
#ifdef _DEBUG
	const int _B = 64;
#endif

	template<typename T, int BLOCK_SIZE, int MULTI_DATA, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT>
	__global__ void kernel_convolution2D(
		const T* __restrict__ src, T* dst, int width, int widthStride, int height,
		const T* __restrict__ weight) {
#if 1
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE*MULTI_DATA - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

		T data[MULTI_DATA][DATA_CACHE_SIZE];

		int tidx = BLOCK_PROCESS_DATA_COUNT*blockIdx.x + WARP_PROCESS_DATA_COUNT*warpId + laneId*MULTI_DATA - FILTER_WIDTH / 2;
		int tidy = PROCESS_DATA_COUNT*blockIdx.y - FILTER_HEIGHT / 2;

		__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0];
		for (int i = threadIdx.x; i < FILTER_HEIGHT*FILTER_WIDTH; i += blockDim.x)
			psmem[i] = weight[i];
		__syncthreads();

//		int index = widthStride*tidy + tidx;
//		//#pragma unroll
//		for (int s = 0; s < DATA_CACHE_SIZE; s++) {
//			int _tidy = tidy + s;
//			if (_tidy < height && _tidy >= 0) {
//				for (int k = 0; k < MULTI_DATA; k++) {
//					int cur_idx = tidx + k;
//					if (cur_idx >= 0 && cur_idx < width) {
//						data[k][s] = src[cur_idx];
//					}
//					else {
//						data[k][s] = 0;
//					}
//				}
//			}
//			else {
//				for (int k = 0; k < MULTI_DATA; k++) {
//					data[k][s] = 0;
//				}
//			}
//			index += widthStride;
//		}
//		T* p = data[0];
//#pragma unroll
//		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
//			T sum = 0;
//#pragma unroll
//			for (int m = 0; m < FILTER_WIDTH; m++) {
//				if (m > 0) {
//					sum = __my_shfl_up(sum, 1);
//				}
//				int a = data[][i + 0];
//				int b = data[i + 1];
//				int c = data[i + 2];
//				int d = data[i + 3];
//				int e = data[i + 4];
//				int f = data[i + 5];
//#pragma unroll
//				for (int n = 0; n < FILTER_HEIGHT; n++) {
//					int a = data[i + 0];
//
//					//sum += data[i + n] * smem[n][m];
//					sum = MAD(data[i + n], smem[n][m], sum);
//				}
//			}
//			data[i] = sum;
//		}
//
//		index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1) / 2;
//#pragma unroll
//		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
//			if (laneId >= FILTER_WIDTH - 1 && tidx - (FILTER_WIDTH - 1) / 2 < width && tidy + FILTER_HEIGHT / 2 + i < height) {
//				dst[index] = data[i];
//			}
//			index += widthStride;
//		}
//		/**/
#endif
	}

	template<>
	__global__ void kernel_convolution2D<float, 
#ifdef _DEBUG
		_B,
#else
		512, 
#endif
		2, 8, 3, 3>(
		const float* __restrict__ src, float* dst, int width, int widthStride, int height,
		const float* __restrict__ weight) {
		typedef float T;
		const int BLOCK_SIZE = blockDim.x;
		const int PROCESS_DATA_COUNT = 8;
		const int MULTI_DATA = 2;
		const int FILTER_WIDTH = 3;
		const int FILTER_HEIGHT = 3;
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE*MULTI_DATA - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

		T data[MULTI_DATA][DATA_CACHE_SIZE];

		int tidx = BLOCK_PROCESS_DATA_COUNT*blockIdx.x + WARP_PROCESS_DATA_COUNT*warpId + laneId*MULTI_DATA - FILTER_WIDTH / 2;
		int tidy = PROCESS_DATA_COUNT*blockIdx.y - FILTER_HEIGHT / 2;

		__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
		T* psmem = &smem[0][0];
		for (int i = threadIdx.x; i < FILTER_HEIGHT*FILTER_WIDTH; i += blockDim.x)
			psmem[i] = weight[i];
		__syncthreads();

		int index = widthStride*tidy + tidx;
		//#pragma unroll
		for (int s = 0; s < DATA_CACHE_SIZE; s++) {
			int _tidy = tidy + s;
			if ( _tidy >= 0 && _tidy < height) {
				for (int k = 0; k < MULTI_DATA; k++) {
					int cur_idx = tidx + k;
					if (cur_idx >= 0 && cur_idx < width) {
						data[k][s] = src[index + k];
					}
					else {
						data[k][s] = 0;
					}
				}
			} else {
				for (int k = 0; k < MULTI_DATA; k++) {
					data[k][s] = 0;
				}
			}
			//T a = data[0][s];
			//T b = data[1][s];
			//printf("%f, %f, ", a, b);
			index += widthStride;
		}

		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
			T sum0 = 0;
			T sum1 = 0;
			T sum;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0) {
					sum = __my_shfl_up(sum1, 1);
					sum1 = sum0;
					sum0 = sum;
				}	
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
					int a = data[0][i + n];
					int b = data[1][i + n];

					T _smem = smem[n][m];
					//sum += data[i + n] * smem[n][m];
					sum0 = MAD(data[0][i + n], _smem, sum0);
					sum1 = MAD(data[1][i + n], _smem, sum1);
				}
			}
			data[0][i] = sum0;
			data[1][i] = sum1;
		}

		index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1) / 2;
#pragma unroll
		for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
			if (tidy + FILTER_HEIGHT / 2 + i < height) {
				int a = data[0][i];
				int b = data[1][i];
				for (int k = 0; k < MULTI_DATA; k++) {
					int cur_idx = tidx - (FILTER_WIDTH - 1) / 2 + k;
					if (laneId*2 + k >= FILTER_WIDTH - 1 && cur_idx >= 0 && cur_idx < width) {
						dst[index + k] = data[k][i];
					}
				}
			}
			index += widthStride;
		}
	}

		template<>
		__global__ void kernel_convolution2D<float,
#ifdef _DEBUG
			_B,
#else
			512,
#endif
			2, 8, 5, 5>(
				const float* __restrict__ src, float* dst, int width, int widthStride, int height,
				const float* __restrict__ weight) {
			typedef float T;
			const int BLOCK_SIZE = blockDim.x;
			const int PROCESS_DATA_COUNT = 8;
			const int MULTI_DATA = 2;
			const int FILTER_WIDTH = 5;
			const int FILTER_HEIGHT = 5;
			const int WARP_COUNT = BLOCK_SIZE >> 5;
			const int laneId = threadIdx.x & 31;
			const int warpId = threadIdx.x >> 5;
			const int WARP_PROCESS_DATA_COUNT = WARP_SIZE*MULTI_DATA - FILTER_WIDTH + 1;
			const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
			const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

			T data[MULTI_DATA][DATA_CACHE_SIZE];

			int tidx = BLOCK_PROCESS_DATA_COUNT*blockIdx.x + WARP_PROCESS_DATA_COUNT*warpId + laneId*MULTI_DATA - FILTER_WIDTH / 2;
			int tidy = PROCESS_DATA_COUNT*blockIdx.y - FILTER_HEIGHT / 2;

			__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
			T* psmem = &smem[0][0];
			for (int i = threadIdx.x; i < FILTER_HEIGHT*FILTER_WIDTH; i += blockDim.x)
				psmem[i] = weight[i];
			__syncthreads();

			int index = widthStride*tidy + tidx;
			//#pragma unroll
			for (int s = 0; s < DATA_CACHE_SIZE; s++) {
				int _tidy = tidy + s;
				if (_tidy >= 0 && _tidy < height) {
					for (int k = 0; k < MULTI_DATA; k++) {
						int cur_idx = tidx + k;
						if (cur_idx >= 0 && cur_idx < width) {
							data[k][s] = src[index + k];
						}
						else {
							data[k][s] = 0;
						}
					}
				}
				else {
					for (int k = 0; k < MULTI_DATA; k++) {
						data[k][s] = 0;
					}
				}
				//T a = data[0][s];
				//T b = data[1][s];
				//printf("%f, %f, ", a, b);
				index += widthStride;
			}

			for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
				T sum0 = 0;
				T sum1 = 0;
				T sum;
#pragma unroll
				for (int m = 0; m < FILTER_WIDTH; m++) {
					if (m > 0) {
						sum = __my_shfl_up(sum1, 1);
						sum1 = sum0;
						sum0 = sum;
					}
#pragma unroll
					for (int n = 0; n < FILTER_HEIGHT; n++) {
						int a = data[0][i + n];
						int b = data[1][i + n];

						T _smem = smem[n][m];
						//sum += data[i + n] * smem[n][m];
						sum0 = MAD(data[0][i + n], _smem, sum0);
						sum1 = MAD(data[1][i + n], _smem, sum1);
					}
				}
				data[0][i] = sum0;
				data[1][i] = sum1;
			}

			index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1) / 2;
#pragma unroll
			for (int i = 0; i < PROCESS_DATA_COUNT; i++) {
				if (tidy + FILTER_HEIGHT / 2 + i < height) {
					int a = data[0][i];
					int b = data[1][i];
					for (int k = 0; k < MULTI_DATA; k++) {
						int cur_idx = tidx - (FILTER_WIDTH - 1) / 2 + k;
						if (laneId * 2 + k >= FILTER_WIDTH - 1 && cur_idx >= 0 && cur_idx < width) {
							dst[index + k] = data[k][i];
						}
					}
				}
				index += widthStride;
			}
		}
	template<int FILTER_WIDTH, int FILTER_HEIGHT, int PROCESS_DATA_COUNT, int BLOCK_SIZE>
	static float Test_conv2D(int width, int height) {
		typedef float DataType;

		//int width =  9216; // 1024 * 8;
		//int height = width;
		//const int FILTER_WIDTH = 3;
		//const int FILTER_HEIGHT = FILTER_WIDTH;
		//const int BLOCK_SIZE = 512;
		//const int PROCESS_DATA_COUNT = 1;

		const int MULTI_DATA = 2;
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE*MULTI_DATA - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		const int DATA_CACHE_SIZE = PROCESS_DATA_COUNT + FILTER_HEIGHT - 1;

		const int nRepeatCount = 3;
		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> img;
		char szPath[1024] = "";
		sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		bool bRtn = false;
		bRtn = img.Load<uchar>(szPath, width, height);
		//sprintf(szPath, "../data/Lena%dx%d.txt", width, height);
		//img.SaveText(szPath);
		if (!bRtn) img.MallocBuffer(width, height);
		for (int i = 0; i < img.width*img.height; i++) img.data[i] = i%img.width+1;
		if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(width, height), devDst(width, height);
		devSrc.CopyFromHost(img.data, img.width, img.width, img.height);
		DataT<DataType> imgDst;
		imgDst.MallocBuffer(width, height);

		dim3 block_size(BLOCK_SIZE, 1);
		dim3 grid_size(UpDivide(width, BLOCK_PROCESS_DATA_COUNT), UpDivide(height, PROCESS_DATA_COUNT));

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ -1.0,  -1.0, -1.0, },
			{ -1.0, 8.0, -1.0, },
			{ -1.0,  -1.0, -1.0, },
		};
#endif

#if 1
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  0.0, },
			{ 0,  -1.0, },
			//			{ -1.0,  -2.0, -1.0, },
		};
#endif

#if 0
		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
			{ 1.0,  1.0, 1.0, },
			{ 1.0, 1.0, 1.0, },
			{ 1.0,  1.0, 1.0, },
		};
#endif
		DataType* flt = &filter[0][0];
		for (int i = 0; i < FILTER_HEIGHT*FILTER_WIDTH; i++)
			flt[i] = -1;
		flt[FILTER_HEIGHT*FILTER_WIDTH / 2] = FILTER_HEIGHT*FILTER_WIDTH - 1;
		//filter[0][0] = 1;
		//filter[FILTER_HEIGHT-1][FILTER_WIDTH-1] = -1;
		//for (int i = 0; i < FILTER_HEIGHT / 2; i++) {
		//	for (int j = 0; j < FILTER_WIDTH; j++) {
		//		filter[FILTER_HEIGHT - 1 - i][j] = 1;
		//		filter[i][j] = -1;
		//	}
		//}

		DevData<DataType> devFilter(FILTER_HEIGHT*FILTER_WIDTH);
		devFilter.CopyFromHost(&filter[0][0], FILTER_WIDTH*FILTER_HEIGHT, FILTER_WIDTH*FILTER_HEIGHT, 1);

		cudaEventRecord(start, 0);
		//watch.start();
		//Conv2D(devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData(), FILTER_WIDTH, FILTER_HEIGHT);
		for (int s = 0; s < nRepeatCount; s++) {
			kernel_convolution2D<DataType, BLOCK_SIZE, MULTI_DATA, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT> << <grid_size, block_size >> > (devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData());
			cudaDeviceSynchronize();
		}
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(imgDst.data, imgDst.width, imgDst.width, imgDst.height);

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		inc /= (float)nRepeatCount;
		printf("%dx%d , %dx%d , proc_count=%d, BLOCK_SIZE=%d, %f ms , %f fps\n", width, height, FILTER_WIDTH, FILTER_HEIGHT, PROCESS_DATA_COUNT, BLOCK_SIZE, inc, 1000.0 / inc);
		sprintf(szPath, "../data/Lena_proc_multi_%dx%d.raw", width, height);
		//imgDst.SaveRaw(szPath);

		sprintf(szPath, "../data/Lena_proc_multi_%dx%d.txt", width, height);
		//imgDst.SaveText(szPath);

		DataT<DataType> imgVerify;
		imgVerify.MallocBuffer(width, height);
		Convolution(img.data, imgVerify.data, width, height, filter[0], FILTER_WIDTH, FILTER_HEIGHT);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);

		double dif = 0;
		for (int i = 0; i < img.width*img.height; i++) {
			dif += abs(imgVerify.data[i] - imgDst.data[i]);
		}
		printf("verify dif =%f\n", dif);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);
		sprintf(szPath, "../data/Lena_proc_verify(%dx%d)_%dx%d.raw", FILTER_WIDTH, FILTER_HEIGHT, width, height);
		//imgVerify.SaveRaw(szPath);

		FILE* fp = fopen("log.conv2D.csv", "at");
		if (fp) {
			fprintf(fp, "%dx%d, %dx%d, %f\n", width, height, FILTER_WIDTH, FILTER_HEIGHT, inc);
			fclose(fp);
		}
		return inc;
	}

};

static void Test_2D_Diff_Cache() {
	/*
	int width = 9216;
	int height = width;
	const int BLOCK_SIZE = 512;
	#if 0
	{
	{
	const int size = 3;
	CONV2D::Test_conv2D<size, size, 1>(width, height);
	CONV2D::Test_conv2D<size, size, 2>(width, height);
	CONV2D::Test_conv2D<size, size, 3>(width, height);
	CONV2D::Test_conv2D<size, size, 4>(width, height);
	CONV2D::Test_conv2D<size, size, 5>(width, height);
	CONV2D::Test_conv2D<size, size, 6>(width, height);
	CONV2D::Test_conv2D<size, size, 7>(width, height);
	CONV2D::Test_conv2D<size, size, 8>(width, height);
	CONV2D::Test_conv2D<size, size, 9>(width, height);
	CONV2D::Test_conv2D<size, size, 10>(width, height);
	CONV2D::Test_conv2D<size, size, 11>(width, height);
	CONV2D::Test_conv2D<size, size, 12>(width, height);
	CONV2D::Test_conv2D<size, size, 13>(width, height);
	CONV2D::Test_conv2D<size, size, 14>(width, height);
	CONV2D::Test_conv2D<size, size, 15>(width, height);
	CONV2D::Test_conv2D<size, size, 16>(width, height);
	CONV2D::Test_conv2D<size, size, 17>(width, height);
	}
	}
	{
	const int size = 5;
	CONV2D::Test_conv2D<size, size, 1>(width, height);
	CONV2D::Test_conv2D<size, size, 2>(width, height);
	CONV2D::Test_conv2D<size, size, 3>(width, height);
	CONV2D::Test_conv2D<size, size, 4>(width, height);
	CONV2D::Test_conv2D<size, size, 5>(width, height);
	CONV2D::Test_conv2D<size, size, 6>(width, height);
	CONV2D::Test_conv2D<size, size, 7>(width, height);
	CONV2D::Test_conv2D<size, size, 8>(width, height);
	CONV2D::Test_conv2D<size, size, 9>(width, height);
	CONV2D::Test_conv2D<size, size, 10>(width, height);
	CONV2D::Test_conv2D<size, size, 11>(width, height);
	CONV2D::Test_conv2D<size, size, 12>(width, height);
	CONV2D::Test_conv2D<size, size, 13>(width, height);
	CONV2D::Test_conv2D<size, size, 14>(width, height);
	CONV2D::Test_conv2D<size, size, 15>(width, height);
	CONV2D::Test_conv2D<size, size, 16>(width, height);
	CONV2D::Test_conv2D<size, size, 17>(width, height);
	}
	{
	{
	{
	const int size = 7;
	CONV2D::Test_conv2D<size, size, 1>(width, height);
	CONV2D::Test_conv2D<size, size, 2>(width, height);
	CONV2D::Test_conv2D<size, size, 3>(width, height);
	CONV2D::Test_conv2D<size, size, 4>(width, height);
	CONV2D::Test_conv2D<size, size, 5>(width, height);
	CONV2D::Test_conv2D<size, size, 6>(width, height);
	CONV2D::Test_conv2D<size, size, 7>(width, height);
	CONV2D::Test_conv2D<size, size, 8>(width, height);
	CONV2D::Test_conv2D<size, size, 9>(width, height);
	CONV2D::Test_conv2D<size, size, 10>(width, height);
	CONV2D::Test_conv2D<size, size, 11>(width, height);
	CONV2D::Test_conv2D<size, size, 12>(width, height);
	CONV2D::Test_conv2D<size, size, 13>(width, height);
	CONV2D::Test_conv2D<size, size, 14>(width, height);
	CONV2D::Test_conv2D<size, size, 15>(width, height);
	CONV2D::Test_conv2D<size, size, 16>(width, height);
	CONV2D::Test_conv2D<size, size, 17>(width, height);
	}
	}
	}
	{
	{
	{
	const int size = 9;
	CONV2D::Test_conv2D<size, size, 1>(width, height);
	CONV2D::Test_conv2D<size, size, 2>(width, height);
	CONV2D::Test_conv2D<size, size, 3>(width, height);
	CONV2D::Test_conv2D<size, size, 4>(width, height);
	CONV2D::Test_conv2D<size, size, 5>(width, height);
	CONV2D::Test_conv2D<size, size, 6>(width, height);
	CONV2D::Test_conv2D<size, size, 7>(width, height);
	CONV2D::Test_conv2D<size, size, 8>(width, height);
	CONV2D::Test_conv2D<size, size, 9>(width, height);
	CONV2D::Test_conv2D<size, size, 10>(width, height);
	CONV2D::Test_conv2D<size, size, 11>(width, height);
	CONV2D::Test_conv2D<size, size, 12>(width, height);
	CONV2D::Test_conv2D<size, size, 13>(width, height);
	CONV2D::Test_conv2D<size, size, 14>(width, height);
	CONV2D::Test_conv2D<size, size, 15>(width, height);
	CONV2D::Test_conv2D<size, size, 16>(width, height);
	CONV2D::Test_conv2D<size, size, 17>(width, height);
	}
	}
	}
	{
	{
	{
	const int size = 11;
	CONV2D::Test_conv2D<size, size, 1>(width, height);
	CONV2D::Test_conv2D<size, size, 2>(width, height);
	CONV2D::Test_conv2D<size, size, 3>(width, height);
	CONV2D::Test_conv2D<size, size, 4>(width, height);
	CONV2D::Test_conv2D<size, size, 5>(width, height);
	CONV2D::Test_conv2D<size, size, 6>(width, height);
	CONV2D::Test_conv2D<size, size, 7>(width, height);
	CONV2D::Test_conv2D<size, size, 8>(width, height);
	CONV2D::Test_conv2D<size, size, 9>(width, height);
	CONV2D::Test_conv2D<size, size, 10>(width, height);
	CONV2D::Test_conv2D<size, size, 11>(width, height);
	CONV2D::Test_conv2D<size, size, 12>(width, height);
	CONV2D::Test_conv2D<size, size, 13>(width, height);
	CONV2D::Test_conv2D<size, size, 14>(width, height);
	CONV2D::Test_conv2D<size, size, 15>(width, height);
	CONV2D::Test_conv2D<size, size, 16>(width, height);
	CONV2D::Test_conv2D<size, size, 17>(width, height);
	}
	}
	}
	{
	{
	{
	const int size = 13;
	CONV2D::Test_conv2D<size, size, 1>(width, height);
	CONV2D::Test_conv2D<size, size, 2>(width, height);
	CONV2D::Test_conv2D<size, size, 3>(width, height);
	CONV2D::Test_conv2D<size, size, 4>(width, height);
	CONV2D::Test_conv2D<size, size, 5>(width, height);
	CONV2D::Test_conv2D<size, size, 6>(width, height);
	CONV2D::Test_conv2D<size, size, 7>(width, height);
	CONV2D::Test_conv2D<size, size, 8>(width, height);
	CONV2D::Test_conv2D<size, size, 9>(width, height);
	CONV2D::Test_conv2D<size, size, 10>(width, height);
	CONV2D::Test_conv2D<size, size, 11>(width, height);
	CONV2D::Test_conv2D<size, size, 12>(width, height);
	CONV2D::Test_conv2D<size, size, 13>(width, height);
	CONV2D::Test_conv2D<size, size, 14>(width, height);
	CONV2D::Test_conv2D<size, size, 15>(width, height);
	CONV2D::Test_conv2D<size, size, 16>(width, height);
	CONV2D::Test_conv2D<size, size, 17>(width, height);
	}
	}
	}
	#else
	{
	const int size = 15;
	CONV2D::Test_conv2D<size, size, 9, BLOCK_SIZE>(width, height);
	}
	#endif
	*/
}


//Access Global Memory directly
void Test_2D_multi(int argc, char** argv) {
	DISPLAY_FUNCTION("evaluate");


#if 0 // _DEBUG
	const int PROCESS_DATA_COUNT = 5;
	int width = 9216;
	int height = width;

	CONV2D::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	//CONV2D::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	//CONV2D::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	return;

	Test_2D_Diff_Cache();
	return;
	//for (int i = 0; i<100; i++)
	CONV2D::Test_conv2D<2, 2, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<4, 4, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<6, 6, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<8, 8, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<9, 9, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<10, 10, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<11, 11, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<12, 12, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<13, 13, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<14, 14, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<15, 15, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<16, 16, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<17, 17, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<18, 18, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<19, 19, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<20, 20, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<21, 21, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<22, 22, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<23, 23, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<24, 24, PROCESS_DATA_COUNT>(width, height);
	/*	CONV2D::Test_conv2D<25, 25, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<26, 26, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<27, 27, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<28, 28, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<29, 29, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<30, 30, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<31, 31, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<32, 32, PROCESS_DATA_COUNT>(width, height);*/

#endif

#if 0
	int width = 1024;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;
	const int PROCESS_DATA_COUNT = 8;
	const int BLOCK_SIZE = 512;
	//for (int i = 0; i<100; i++)
	{ const int FilterSize = 2;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 4;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 5;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 6;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 7;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 8;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 9;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 10;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 11;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 12;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 13;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 14;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 15;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 16;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 17;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 18;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 19;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 20;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 21;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 22;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 23;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 24;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 25;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 26;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 27;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 28;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 29;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 30;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 31;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 32;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
#endif

#if 1
	int width = 512;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;

#ifdef _DEBUG
	const int BLOCK_SIZE = CONV2D_multi::_B;
#else
	const int BLOCK_SIZE = 512;
#endif
	//for (int i = 0; i<100; i++)
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 1; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 2; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 3; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 4; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 5; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 6; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 7; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 8; CONV2D_multi::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 5;  const int PROCESS_DATA_COUNT = 8; CONV2D_multi::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }

	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 9; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 10; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 11; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 12; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 13; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 14; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 15; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 16; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 17; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 18; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 19; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 20; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 21; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 32; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 23; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 24; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 25; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 26; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 27; CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }

#endif
}




