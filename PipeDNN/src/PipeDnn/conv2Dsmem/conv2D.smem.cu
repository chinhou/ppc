#include "../conv/common.h"
#include "../conv/cudaLib.cuh"

namespace CONV2D_smem {
	static const int WARP_SIZE = 32;

	template<typename T, int BLOCK_SIZE, int DATA_CACHE_SIZE, int FILTER_WIDTH, int FILTER_HEIGHT>
	__global__ void kernel_convolution2D(
		const T* __restrict__ src, T* dst, int width, int widthStride, int height,
		const T* __restrict__ weight) {
		assert(DATA_CACHE_SIZE >= FILTER_HEIGHT);
#if 1
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		//const int DATA_CACHE_SIZE = FILTER_HEIGHT + PROCESS_DATA_COUNT - 1;
		const int BLOCK_PROCESS_DATA_ROWS = DATA_CACHE_SIZE*WARP_COUNT - FILTER_HEIGHT + 1;
		const int laneId = threadIdx.x & 31;
		const int warpId = threadIdx.x >> 5;
		const int PROCESS_DATA_COUNT = DATA_CACHE_SIZE + FILTER_HEIGHT - 1;

		T data[DATA_CACHE_SIZE];

		int tidx = WARP_PROCESS_DATA_COUNT*blockIdx.x + threadIdx.x - FILTER_WIDTH / 2;
		int tidy = BLOCK_PROCESS_DATA_ROWS*blockIdx.y + DATA_CACHE_SIZE*threadIdx.y - FILTER_HEIGHT / 2;

		__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
		__shared__ T sdata[WARP_COUNT][FILTER_HEIGHT - 1][WARP_SIZE];
		T* psmem = &smem[0][0];

		assert(blockDim.x == WARP_SIZE);
		assert(blockDim.y == WARP_COUNT);
		int tx = threadIdx.x + threadIdx.y*blockDim.x;
		for (int i = tx; i < FILTER_HEIGHT*FILTER_WIDTH; i += BLOCK_SIZE)
			psmem[i] = weight[i];

		int index = widthStride*tidy + tidx;
#pragma unroll
		for (int s = 0; s < DATA_CACHE_SIZE; s++) {
			int _tidy = tidy + s;
			if (tidx >= 0 && tidx < width && _tidy >= 0 && _tidy < height) {
				data[s] = src[index];
			}else{
				data[s] = 0;
			}
			index += widthStride;
		}
		if (threadIdx.y > 0) {
#pragma unroll
			for (int s = 0; s < FILTER_HEIGHT - 1; s++) {
				sdata[threadIdx.y-1][s][threadIdx.x] = data[s];
			}
		}
		else {
#pragma unroll
			for (int s = 0; s < FILTER_HEIGHT - 1; s++) {
				sdata[blockDim.y-1][s][threadIdx.x] = 0;
			}
		}
		__syncthreads();

		//T* p = &data[0];
#pragma unroll
		for (int i = 0; i < DATA_CACHE_SIZE; i++) {
			T sum = 0;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0) {
					sum = __my_shfl_up(sum, 1);
				}
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
					if (i + n < DATA_CACHE_SIZE) {
						//T a = data[i + 0];
						//T b = data[i + 1];
						//T c = data[i + 2];
						//T d = data[i + 3];
						sum = MAD(data[i + n], smem[n][m], sum);
					} else {
						sum = MAD(sdata[threadIdx.y][i + n - DATA_CACHE_SIZE][threadIdx.x], smem[n][m], sum);
					}
				}
			}
			data[i] = sum;
		}

		index = widthStride*(tidy + FILTER_HEIGHT / 2) + tidx - (FILTER_WIDTH - 1) / 2;
#pragma unroll
		for (int i = 0; i < DATA_CACHE_SIZE; i++) {
			if (threadIdx.x >= FILTER_WIDTH - 1 && tidx - (FILTER_WIDTH - 1) / 2 < width && tidy + FILTER_HEIGHT / 2 + i < height) {
				if (threadIdx.y == blockDim.y - 1) {
					if (i < DATA_CACHE_SIZE - FILTER_HEIGHT + 1) {
						dst[index] = data[i];
					}
				} else {
					dst[index] = data[i];
				}
			}
			index += widthStride;
		}
#endif
	}

	template<typename T, int BLOCK_SIZE, int PROCESS_DATA_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT> inline
		void Convolution2D(const T* src, T* dst, int width, int widthPitch, int height, T* filter, int filter_width, int filter_height) {
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		dim3 block_size(BLOCK_SIZE, 1);
		dim3 grid_size(UpDivide(width, BLOCK_PROCESS_DATA_COUNT), UpDivide(height, PROCESS_DATA_COUNT));
#define MyKernel kernel_convolution2D<T, BLOCK_SIZE, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT>
		cudaFuncSetCacheConfig(MyKernel, cudaFuncCachePreferL1);
		MyKernel << <grid_size, block_size >> > (src, dst, width, widthPitch, height, filter);
#undef MyKernel
	}

	template<typename T, int FILTER_WIDTH, int FILTER_HEIGHT> inline
		void _Conv2D(const T* src, T* dst, int width, int widthPitch, int height, T* filter) {
		static const int cBlockSize[] = {
			1024, 1024, 1024, 1024, 1024, 960, 960, 960, 960, 960, 960, 960, 960, 960, 960,
		};
		assert(FILTER_WIDTH == FILTER_HEIGHT);
		const int BLOCK_SIZE = FILTER_WIDTH <= 5 ? 1024 : 512;
		const int PROCESS_DATA_COUNT = 8;
		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int BLOCK_PROCESS_DATA_COUNT = WARP_PROCESS_DATA_COUNT*WARP_COUNT;
		Convolution2D<T, BLOCK_SIZE, PROCESS_DATA_COUNT, FILTER_WIDTH, FILTER_HEIGHT>(src, dst, width, widthPitch, height, filter, FILTER_WIDTH, FILTER_HEIGHT);
	}

	template<typename T> inline
		void Conv2D(const T* src, T* dst, int width, int widthPitch, int height, T* filter, int filter_width, int filter_height) {
		switch (filter_width)
		{
#define CASE_CONV(_x) case _x: _Conv2D<T, _x, _x>(src, dst, width, widthPitch, height, filter); break;	
			CASE_CONV(2);
			CASE_CONV(3);
			CASE_CONV(4);
			CASE_CONV(5);
			CASE_CONV(6);
			CASE_CONV(7);
			CASE_CONV(8);
			CASE_CONV(9);
			CASE_CONV(10);
			CASE_CONV(11);
			CASE_CONV(12);
			CASE_CONV(13);
			CASE_CONV(14);
			CASE_CONV(15);
			CASE_CONV(16);
			CASE_CONV(17);
			CASE_CONV(18);
			CASE_CONV(19);
			CASE_CONV(20);
			CASE_CONV(21);
			CASE_CONV(22);
			CASE_CONV(23);
			CASE_CONV(24);
			CASE_CONV(25);
			CASE_CONV(26);
			CASE_CONV(27);
			CASE_CONV(28);
			CASE_CONV(29);
			CASE_CONV(30);
			CASE_CONV(31);
			CASE_CONV(32);
		default:
			break;
		}

	}

	template<int FILTER_WIDTH, int FILTER_HEIGHT, int PROCESS_DATA_COUNT, int BLOCK_SIZE>
	static float Test_conv2D(int width, int height) {
		typedef float DataType;

		//int width =  9216; // 1024 * 8;
		//int height = width;
		//const int FILTER_WIDTH = 3;
		//const int FILTER_HEIGHT = FILTER_WIDTH;
		//const int BLOCK_SIZE = 512;
		//const int PROCESS_DATA_COUNT = 1;

		const int WARP_COUNT = BLOCK_SIZE >> 5;
		const int WARP_PROCESS_DATA_COUNT = WARP_SIZE - FILTER_WIDTH + 1;
		const int DATA_CACHE_SIZE = FILTER_HEIGHT + PROCESS_DATA_COUNT - 1;
		const int BLOCK_PROCESS_DATA_ROWS = DATA_CACHE_SIZE*WARP_COUNT - FILTER_HEIGHT + 1;

		const int nRepeatCount = 1;
		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		//StopWatchWin watch;
		DataT<DataType> img;
		char szPath[1024] = "";
		sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		bool bRtn = false;
		bRtn = img.Load<uchar>(szPath, width, height);
		//sprintf(szPath, "../data/Lena%dx%d.txt", width, height);
		//img.SaveText(szPath);
		if (!bRtn) img.MallocBuffer(width, height);
		//for (int i = 0; i < img.width*img.height; i++) img.data[i] = i / img.width + 1;
		if (!bRtn) printf("Load failed : %s\n", szPath);
		DevData<DataType> devSrc(width, height), devDst(width, height);
		devSrc.CopyFromHost(img.data, img.width, img.width, img.height);
		DataT<DataType> imgDst;
		imgDst.MallocBuffer(width, height);

		dim3 block_size(WARP_SIZE, WARP_COUNT);
		dim3 grid_size(UpDivide(width, WARP_PROCESS_DATA_COUNT), UpDivide(height, BLOCK_PROCESS_DATA_ROWS));

		DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = { { 0, } };
		DataType* flt = &filter[0][0];
		for (int i = 0; i < FILTER_HEIGHT*FILTER_WIDTH; i++)
			flt[i] = -1;
		flt[FILTER_HEIGHT*FILTER_WIDTH / 2] = FILTER_HEIGHT*FILTER_WIDTH - 1;

		DevData<DataType> devFilter(FILTER_HEIGHT*FILTER_WIDTH);
		devFilter.CopyFromHost(&filter[0][0], FILTER_WIDTH*FILTER_HEIGHT, FILTER_WIDTH*FILTER_HEIGHT, 1);

		cudaEventRecord(start, 0);
		//watch.start();
		//Conv2D(devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData(), FILTER_WIDTH, FILTER_HEIGHT);
		for (int s = 0; s < nRepeatCount; s++) {
			kernel_convolution2D<DataType, BLOCK_SIZE, DATA_CACHE_SIZE, FILTER_WIDTH, FILTER_HEIGHT> << <grid_size, block_size >> > (devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData());
			cudaDeviceSynchronize();
		}
		//watch.stop();
		cudaEventRecord(stop, 0);
		cudaEventSynchronize(stop);
		CUDA_CHECK_ERROR;

		devDst.CopyToHost(imgDst.data, imgDst.width, imgDst.width, imgDst.height);

		cudaEventElapsedTime(&inc, start, stop);
		//inc = watch.getAverageTime();
		inc /= (float)nRepeatCount;
		printf("%dx%d , %dx%d , proc_count=%d, BLOCK_SIZE=%d, %f ms , %f fps\n", width, height, FILTER_WIDTH, FILTER_HEIGHT, PROCESS_DATA_COUNT, BLOCK_SIZE, inc, 1000.0 / inc);
		sprintf(szPath, "../data/Lena_proc_%dx%d.raw", width, height);
		imgDst.SaveRaw(szPath);

		sprintf(szPath, "../data/Lena_proc_%dx%d.txt", width, height);
		//imgDst.SaveText(szPath);

		DataT<DataType> imgVerify;
		imgVerify.MallocBuffer(width, height);
		Convolution(img.data, imgVerify.data, width, height, filter[0], FILTER_WIDTH, FILTER_HEIGHT);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);

		double dif = 0;
		for (int i = 0; i < img.width*img.height; i++) {
			dif += abs(imgVerify.data[i] - imgDst.data[i]);
		}
		printf("verify dif =%f\n", dif);
		sprintf(szPath, "../data/Lena_proc_verify_%dx%d.txt", width, height);
		//imgVerify.SaveText(szPath);
		sprintf(szPath, "../data/Lena_proc_verify(%dx%d)_%dx%d.raw", FILTER_WIDTH, FILTER_HEIGHT, width, height);
		//imgVerify.SaveRaw(szPath);

		FILE* fp = fopen("log.conv2D.csv", "at");
		if (fp) {
			fprintf(fp, "%dx%d, %dx%d, %f\n", width, height, FILTER_WIDTH, FILTER_HEIGHT, inc);
			fclose(fp);
		}
		return inc;
	}
};

void Test_2D_smem(int argc, char** argv) {
	DISPLAY_FUNCTION("evaluate");
	{
		int width = 1024;
		int height = width;
		const int BLOCK_SIZE = 512;
		//for (int i = 0; i<100; i++)
		//{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 8; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	}
#if 0 // _DEBUG
	const int PROCESS_DATA_COUNT = 5;
	int width = 9216;
	int height = width;

	CONV2D::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	//CONV2D::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	//CONV2D::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	return;

	Test_2D_Diff_Cache();
	return;
	//for (int i = 0; i<100; i++)
	CONV2D::Test_conv2D<2, 2, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<3, 3, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<4, 4, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<5, 5, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<6, 6, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<7, 7, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<8, 8, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<9, 9, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<10, 10, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<11, 11, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<12, 12, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<13, 13, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<14, 14, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<15, 15, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<16, 16, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<17, 17, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<18, 18, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<19, 19, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<20, 20, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<21, 21, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<22, 22, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<23, 23, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<24, 24, PROCESS_DATA_COUNT>(width, height);
	/*	CONV2D::Test_conv2D<25, 25, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<26, 26, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<27, 27, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<28, 28, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<29, 29, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<30, 30, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<31, 31, PROCESS_DATA_COUNT>(width, height);
	CONV2D::Test_conv2D<32, 32, PROCESS_DATA_COUNT>(width, height);*/

#endif

#if 0
	int width = 1024;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;
	const int PROCESS_DATA_COUNT = 8;
	const int BLOCK_SIZE = 512;
	//for (int i = 0; i<100; i++)
	{ const int FilterSize = 2;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 4;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 5;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 6;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 7;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 8;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 9;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 10;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 11;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 12;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 13;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 14;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 15;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 16;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 17;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 18;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 19;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 20;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 384>(width, height); }
	{ const int FilterSize = 21;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 22;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 23;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 24;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 25;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 26;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 27;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 28;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 29;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 30;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 31;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
	{ const int FilterSize = 32;  CONV2D::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, 256>(width, height); }
#endif

#if 1
	int width = 8192;
	if (argc > 1) width = atoi(argv[1]);
	int height = width;

	const int BLOCK_SIZE = 512;
	//for (int i = 0; i<100; i++)
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 1; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 2; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 3; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 4; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 5; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 6; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 7; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 8; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 9; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 10; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 11; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 12; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 13; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 14; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 15; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 16; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 17; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 18; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 19; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 20; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 21; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 32; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 23; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 24; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 25; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 26; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }
	{ const int FilterSize = 3;  const int PROCESS_DATA_COUNT = 27; CONV2D_smem::Test_conv2D<FilterSize, FilterSize, PROCESS_DATA_COUNT, BLOCK_SIZE>(width, height); }

#endif
}




