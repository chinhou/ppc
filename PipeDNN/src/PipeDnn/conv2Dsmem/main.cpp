#include <stdio.h>
#include <iostream>


int main(int argc, char** argv) {
	extern void Test_2D(int argc, char** argv);
	extern void Test_conv2D();
	extern void Test_01_01();
	extern void Test_01_01_01();
	extern void Test_01_01_02();
	extern void Test_01_01_03();
	extern void Test_01_01_04();
	extern void Test_01_02();
	extern void Test_01_03();
	extern void Test_01_04();
	extern void Test_2D_SharedMem();
	extern void Test_2D_GlobalMem();
	extern void Test_2D_F3F5(int argc, char** argv);
	extern void Test_2D_smem(int argc, char** argv);
	Test_2D_smem(argc, argv);
	//Test_2D_F3F5(argc, argv);
	//Test_2D_SharedMem();
	//Test_2D_GlobalMem();
	//Test_conv2D();
	//Test_01_01();
	//Test_01_01_01();
	//Test_01_01_02();
	//Test_01_01_03();
	//Test_01_01_04();
	//Test_01_02();
	//Test_01_03();
	//Test_01_04();

	return 0;
}