#ifndef __CONVLAYER_H
#define __CONVLAYER_H

#ifdef _WIN32
#pragma once
#include <Windows.h>
#endif

#include <stdio.h>
#include <string.h>
#include <vector>
#include "im2col.h"
#include "gemm.h"

template<typename T>
struct DataT {
	DataT(int w=0, int h = 0, int c = 0, int n = 0) :data(NULL),width(w), height(h), channels(c), count(n) {
		if (w*h*c*n > 0) MallocBuffer(w, h, c, n);
	}
	inline DataT& MallocBuffer(int w, int h, int c, int n) {
		_mem.resize(w*h*c*n);
		data = !_mem.empty() ? &_mem[0] : NULL;
		width = w;
		height = h;
		channels = c;
		count = n;
		return *this;
	}
	inline size_t Size() const {
		return width*height*channels*count;
	}
	inline bool SaveRaw(const char* szPath) {
		bool bRtn = false;
		if (data) {
			FILE* fp = fopen(szPath, "wb");
			if (fp) {
				bRtn = width*height*channels*count == fwrite(data, sizeof(data[0]), width*height*channels*count, fp) ? true : false;
				fclose(fp);
			}
		}
		return bRtn;
	}
	void SaveText(const char* szPath) {
		FILE* fp = fopen(szPath, "wt");
		if (fp) {
			int s = 0;
			fprintf(fp, "{\n");
			for (int i = 0; i < count; i++) {
				fprintf(fp, " {\n");
				for (int j = 0; j < channels; j++) {
					fprintf(fp, "  {\n");
					for (int m = 0; m < height; m++) {
						fprintf(fp, "   {");
						for (int n = 0; n < width; n++) {
							fprintf(fp, "%f ", data[s++]);
						}
						fprintf(fp, "   }\n");
					}
					fprintf(fp, "   }\n");
				}
				fprintf(fp, " }\n");
			}
			fprintf(fp, "}\n");
			fclose(fp);
		}
	}
	template<typename D>
	inline bool Load(const char* szPath, int w, int h, int c, int n) {
		bool bRtn = false;
		std::vector<D> vec(w*h*c*n);
		FILE* fp = fopen(szPath, "rb");
		if (fp) {
			if (w*h == fread(&vec[0], sizeof(vec[0]), w*h*c*n, fp)) {
				width    = w;
				height   = h;
				channels = c;
				count = n;
				bRtn = true;
			}
			fclose(fp);
		}
		_mem.clear();
		_mem.resize(vec.size());
		if (bRtn) {
			width = w;
			height = h;
			channels = c;
			count = n;
			for (int i = 0; i < vec.size(); i++)
				_mem[i] = vec[i];
		}
		data = !_mem.empty() ? &_mem[0] : NULL;
		return bRtn;
	}
	T* data;
	int width, height, channels, count;
private:
	std::vector<T> _mem;
};

template<typename T>
struct Convlayer {
	Convlayer() {
	}
	virtual ~Convlayer() {
	}
	Convlayer& Init(const DataT<T>& _input, const DataT<T>& _weight, int _stride, int _pad) {
		input = &_input;
		weight = &_weight;
		stride = _stride;
		pad = _pad;

		int w = get_out_width();
		int h = get_out_height();
		int c = weight->count;
		int n = input->count;
		output.MallocBuffer(w, h, c, n);
		workspace.MallocBuffer(get_workspace_size(), 1, 1, 1);
		return *this;
	}
	void Forward() {
		int out_h = output.height;
		int out_w = output.width;
		int m = output.channels;
		int k = weight->width*weight->height*weight->channels;
		int n = output.width*output.height/**output.channels*/;

		float *a = weight->data;
		float *b = workspace.data;
		float *c = output.data;

		im2col_cpu(input->data, input->channels, input->height, input->width, weight->width, stride, pad, workspace.data);
		gemm(0, 0, m, n, k, 1, a, k, b, n, 1, c, n);
		//for (int i = 0; i < output.channels; i++) 
		//{
		//	gemm(0, 0, m, n, k, 1, a + weight->width*weight->height*weight->channels*i, k, b, n, 1, c + output.width*output.height*i, n);
		//}
	}
	int get_out_width() {
		return (input->width - weight->width + 2 * pad) / stride + 1;
	}
	int get_out_height() {
		return (input->height - weight->height + 2 * pad) / stride + 1;
	}
	int get_workspace_size() {
		return weight->width*weight->height*weight->channels*output.width*output.height*output.channels*input->count;
	}
	DataT<T> workspace;
	const DataT<T>* weight;
	const DataT<T>* input;
	DataT<T> output;
	int stride, pad;
};

static void Test1() {
	typedef float DataType;
	DataT<DataType> src, weight;
	{	
		src.MallocBuffer(5, 5, 3, 1);
		DataType data[] = { 
			1,1,1,1,1,
			1,1,1,1,1,
			1,1,1,1,1,
			1,1,1,1,1,
			1,1,1,1,1,
			2,2,2,2,2,
			2,2,2,2,2,
			2,2,2,2,2,
			2,2,2,2,2,
			2,2,2,2,2,
			3,3,3,3,3,
			3,3,3,3,3,
			3,3,3,3,3,
			3,3,3,3,3,
			3,3,3,3,3 };
		memcpy(src.data, data, sizeof(data[0])*src.width*src.height*src.channels*src.count);
	}
	{
		DataType data[] = { 
			1,1,
			1,1,
			2,2,
			2,2,
			3,3,
			3,3,
		};
		weight.MallocBuffer(2, 2, 3, 1);
		memcpy(weight.data, data, sizeof(data));
	}
	{
		const DataType output[] = {
			56,56,56,56,
			56,56,56,56,
			56,56,56,56,
			56,56,56,56,
		};
	}
	Convlayer<DataType> conv;
	conv.Init(src, weight, 1, 1);
	conv.Forward();
	conv.output.SaveText("d:/conv1.txt");

}


static void Test2() {
	typedef float DataType;
	DataT<DataType> src, weight;
	const int minibatch_size = 1;
	const int feature_num = 2;
	const int filter_num = 3;
	const int in_size = 8;
	const int filter_size = 5;
	{
		src.MallocBuffer(in_size, in_size, feature_num, minibatch_size);
		float srcData[minibatch_size][feature_num][in_size][in_size] = {
			{
				{ { 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 1, 0 },{ 1, 1, 0, 0, 0, 0, 0, 1 },{ 0, 1, 0, 1, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 } },
				{ { 1, 1, 1, 1, 1, 1, 1, 1 },{ 1, 1, 1, 1, 1, 1, 1, 1 },{ 1, 1, 1, 1, 1, 1, 1, 1 },{ 1, 1, 1, 1, 1, 1, 1, 1 },{ 1, 1, 1, 1, 1, 1, 1, 1 },{ 1, 1, 1, 1, 1, 1, 1, 1 },{ 1, 1, 1, 1, 1, 1, 1, 1 },{ 1, 1, 1, 1, 1, 1, 1, 1 } }
			}
		};

		memcpy(src.data, &srcData[0][0][0][0], sizeof(src.data[0])*src.width*src.height*src.channels*src.count);
	}
	{
		float filterData[filter_num][feature_num][filter_size][filter_size] = {
			{
				{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
				{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
			},
			{
				{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
				{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
			},
			{
				{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
				{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
			}
		};
		weight.MallocBuffer(5, 5, 2, 3);
		memcpy(weight.data, &filterData[0][0][0][0], sizeof(weight.data[0])*weight.width*weight.height*weight.channels*weight.count);
	}
	{
		const DataType output[] = {
			56,56,56,56,
			56,56,56,56,
			56,56,56,56,
			56,56,56,56,
		};
	}
	Convlayer<DataType> conv;
	conv.Init(src, weight, 1, 2);
	conv.Forward();
	conv.output.SaveText("d:/conv2.txt");
}


void test_im2col_cpu() {
	typedef float DataType;
	DataT<DataType> src, weight;
	{
		src.MallocBuffer(5, 4, 3, 1);
		size_t sz = src.Size();
		for (size_t i = 0; i < sz; i++) src.data[i] = i + 1;
	}
	{
		DataType data[] = {
			1,1,
			1,1,
			2,2,
			2,2,
			3,3,
			3,3,
		};
		weight.MallocBuffer(2, 2, 3, 1);
		memcpy(weight.data, data, sizeof(data));
	}
	Convlayer<DataType> conv;
	conv.Init(src, weight, 1, 0);
	conv.Forward();
	conv.workspace.SaveText("d:/workspace.txt");
}

#endif // !__CONVLAYER_H


