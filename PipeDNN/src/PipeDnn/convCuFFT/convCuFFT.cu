#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#  pragma warning(disable:4819)
#include "../conv/StopWatch.h"
#endif

#ifdef _WIN32
#pragma comment(lib, "cufft.lib")
//#pragma comment(lib, "cufftw.lib")
#endif

#include <string.h>
#include <fstream>
#include <iostream>

#include <cuda_runtime.h>
#include <cuda.h>
#include <cufft.h>
//#include <cufftw.h>
#include "../conv/common.h"
#include "../conv/cudaLib.cuh"
#include "device_launch_parameters.h"
namespace Conv2D_FFT {

	template<typename _Tp0, typename _Tp1> __device__ __forceinline__
		bool IsInBox(_Tp0 x, _Tp0 y, _Tp0 rect_x, _Tp1 rect_y, _Tp1 rect_w, _Tp1 rect_h) {
		return ((x) >= (rect_x) && (y) >= (rect_y) && (x) <= ((rect_w)-1) && y <= (rect_h)-1) ? true : false;
	}

	template<typename _Tp0, typename _Tp1>
	__global__ void kernelPad(const _Tp0* src, int src_pitch, int src_width, int src_height,
		_Tp1* dst, int dst_pitch, int dst_width, int dst_height) {
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		if (x >= 0 && x < dst_width && y >= 0 && y < dst_height) {
			int offsetX = (dst_width - src_width) / 2;
			int offsetY = (dst_height - src_height) / 2;
			int _x = x - offsetX;
			int _y = y - offsetY;
			//bool bInBox = IsInBox(_x, _y, 0, 0, src_width, src_height);
			if (_x >= 0 && _x <= src_width - 1 && _y >= 0 && _y <= src_height - 1) {
				dst[y*dst_pitch + x] = src[_y*src_pitch + _x];
			}
			else {
				//pad data with constant 0
				dst[y*dst_pitch + x] = 0;
			}
		}
	}

	template<typename _Tp0, typename _Tp1> inline
		void PadData(const _Tp0* src, int src_pitch, int src_width, int src_height, _Tp1* dst, int dst_pitch, int dst_width, int dst_height)
	{
		dim3 blocks(16, 16);
		dim3 grids(UpDivide(dst_width, blocks.x), UpDivide(dst_height, blocks.y));
		kernelPad<_Tp0, _Tp1> << <grids, blocks >> > (src, src_pitch, src_width, src_height, dst, dst_pitch, dst_width, dst_height);
		CUDA_CHECK_ERROR;
	}

	////////////////////////////////////////////////////////////////////
	template<typename Complex>
	__global__ void kernelDotScale(const Complex* __restrict__ src, Complex* filter, Complex* dst, int pitch, int width, int height, float scale)
	{
		int x = blockIdx.x*blockDim.x + threadIdx.x;
		int y = blockIdx.y*blockDim.y + threadIdx.y;
		if (!IsInBox(x, y, 0, 0, width, height)) return;
		int offset = y*pitch + x;
		Complex a = src[offset];
		Complex b = filter[offset];
		Complex& c = dst[offset];
		//inner product
		c.x = (a.x*b.x - a.y*b.y)*scale;
		c.y = (a.x*b.y + a.y*b.x)*scale;
	}

	template<typename Complex> inline
	void DotScale(Complex* src, Complex* filter, Complex* dst, int pitch, int width, int height, float scale)
	{
		dim3 blocks(32, 16);
		dim3 grids(UpDivide(width, blocks.x), UpDivide(height, blocks.y));
		kernelDotScale<Complex> << <grids, blocks >> > (src, filter, dst, pitch, width, height, scale);
		CUDA_CHECK_ERROR;
	}
	/////////////////////////////////////////////////////////////////////
	template<typename T>
	__global__ void kernel_fftshift_2D(const T * __restrict__ src, T *dst, int N1, int N2)
	{
		int i = threadIdx.x + blockDim.x * blockIdx.x;
		int j = threadIdx.y + blockDim.y * blockIdx.y;
		if (i >= 0 && j >= 0 && i < N1 && j < N2) {
			int x = i + N1 / 2;
			int y = j + N2 / 2;
			if (x >= N1) x -= N1;
			if (y >= N2) y -= N2;
			dst[j*N1 + i] = src[y*N1+x];
		}
	}

	template<typename T> inline
		void fftshift_2D(const T* __restrict__ src, T* dst, int width, int height)
	{
		dim3 blocks(32, 16);
		dim3 grids(UpDivide(width, blocks.x), UpDivide(height, blocks.y));
		kernel_fftshift_2D<T> << <grids, blocks >> > (src, dst, width, height);
		CUDA_CHECK_ERROR;
	}
	////////////////////////////////////////////////////////////////////
	//the sizes must be a power of 2.
	inline int GetFftSize(int x) {
		int i = 2;
		while (i < x) {
			i <<= 1;
		}
		return i;
	}
	template<typename T>
	void SaveDevBuffer(DevBuffer<T>& devData, std::string path) {
		cudaDeviceSynchronize();
		ImageT<T> tmp;
		tmp.MallocBuffer(devData.width, devData.height);
		devData.CopyToHost(tmp.data, tmp.width, tmp.width, tmp.height);
		tmp.Save(path.c_str());
	}

	inline float FftFilter2D(const float* __restrict__ src, float* dst, int width, int height, float* filter, int filter_width, int filter_height)
	{
		float inc = 0;
		cudaEvent_t start, stop;
		cudaEventCreate(&start);
		cudaEventCreate(&stop);

		cufftResult nRes;
		const int fft_width = GetFftSize(width);
		const int fft_height = GetFftSize(height);
		DevBuffer<float> devSrc(width, height), devDst(width, height), devFilter(filter_width, filter_height);
		DevBuffer<float> devPadSrc(fft_width, fft_height), 
			devPadDst(fft_width, fft_height),
			devPadDstShift(fft_width, fft_height), 
			devPadFilter(fft_width, fft_height);
		DevBuffer<cufftComplex> devSrcComplex(fft_width, fft_height), 
			devFilterComplex(fft_width, fft_height), 
			devComplex(fft_width, fft_height);
		DevBuffer<cufftComplex>& devDstComplex = devSrcComplex;
		devSrc.CopyFromHost(src, width, width, height);
		devFilter.CopyFromHost(filter, filter_width, filter_width, filter_height);
		//ImageT<float> tmp;
		//tmp.MallocBuffer(width, height);
		//devSrc.CopyToHost(tmp.data, width, width, height);
		//tmp.Save("d:/tmp.raw");
		CUDA_CHECK_ERROR;
		cufftHandle plan_forward = NULL;
		nRes = cufftPlan2d(&plan_forward, fft_height, fft_width, CUFFT_R2C);
		nRes = cufftSetCompatibilityMode(plan_forward, CUFFT_COMPATIBILITY_FFTW_PADDING);
		CUDA_CHECK_ERROR;
		cufftHandle plan_backward = NULL;
		nRes = cufftPlan2d(&plan_backward, fft_width, fft_height, CUFFT_C2R);
		//nRes = cufftSetCompatibilityMode(plan_backward, CUFFT_COMPATIBILITY_FFTW_PADDING);
		CUDA_CHECK_ERROR;
		float scale = 1.0 / (fft_width*fft_height);
		PadData(devFilter.GetData(), devFilter.width, devFilter.DataPitch(), devFilter.height, devPadFilter.GetData(), devPadFilter.width, devPadFilter.DataPitch(), devPadFilter.height);
		PadData(devSrc.GetData(), devSrc.width, devSrc.DataPitch(), devSrc.height, devPadSrc.GetData(), devPadSrc.width, devPadSrc.DataPitch(), devPadSrc.height);
		SaveDevBuffer(devPadSrc, FormatString("d:/devPadSrc.%d.%d.raw", devPadSrc.width, devPadSrc.height));
		SaveDevBuffer(devPadFilter, FormatString("d:/devPadFilter.%d.%d.raw", devPadFilter.width, devPadFilter.height));
		{
			cudaEventRecord(start, 0);
			{
				{
					// FFT 2D
					nRes = cufftExecR2C(plan_forward, devPadSrc.GetData(), devSrcComplex.GetData());
					nRes = cufftExecR2C(plan_forward, devPadFilter.GetData(), devFilterComplex.GetData());
					CUDA_CHECK_ERROR;
					{
						ImageT<cufftComplex> tmp;
						tmp.MallocBuffer(width, height);
						devSrcComplex.CopyToHost(tmp.data, width, width, height);
						//{
						//	ImageT<float> comp;
						//	comp.MallocBuffer(width, height);
						//	for (int i = 0; i < comp.width*comp.height; i++) {
						//		comp.data[i] = sqrt(tmp.data[i].x*tmp.data[i].x + tmp.data[i].y*tmp.data[i].y);
						//	}
						//	comp.Save("d:/comp.raw");
						//}
					}
				}
				DotScale(devSrcComplex.GetData(), devFilterComplex.GetData(), devComplex.GetData(), devSrcComplex.DataPitch(), devSrcComplex.width, devSrcComplex.height, scale);
				{
					//IFFT 2D
					nRes = cufftExecC2R(plan_backward, devComplex.GetData(), devPadDst.GetData());
					CUDA_CHECK_ERROR;
				}
				{
					fftshift_2D<float>(devPadDst.GetData(), devPadDstShift.GetData(), devComplex.width, devComplex.height);
				}
				//SaveDevBuffer(devPadDst, FormatString("d:/devPadDst%d.%d.raw", devPadDst.width, devPadDst.height).c_str());
				//SaveDevBuffer(devPadDstShift, FormatString("d:/devPadDstShift%d.%d.raw", devPadDst.width, devPadDst.height).c_str());
				PadData(devPadDstShift.GetData(), devPadDstShift.width, devPadDstShift.DataPitch(), devPadDstShift.height, devDst.GetData(), devDst.width, devDst.DataPitch(), devDst.height);
				cudaDeviceSynchronize();
				CUDA_CHECK_ERROR;
			}
			cudaEventRecord(stop, 0);
			cudaEventSynchronize(stop);
			CUDA_CHECK_ERROR;

			devDst.CopyToHost(dst, width, width, height, 1);

			cudaEventElapsedTime(&inc, start, stop);
		}
		cufftDestroy(plan_forward);
		cufftDestroy(plan_backward);
		return inc;
	}

	void Test(int img_size = 9216) {
		//std::cout << filter_width << " x " << filter_height << " : ";
		typedef float DataType;
		int RepeatCount = 1;
		int width = img_size; //1024 * 4;// 9216; // 9216; // 1024 * 8;
		int height = width;
		//const int FILTER_WIDTH = 3;
		//const int FILTER_HEIGHT = FILTER_WIDTH;
		int filter_width = 3;
		int filter_height = filter_width;

		DataT<DataType> img, dst;
		img.MallocBuffer(width, height);
		dst.MallocBuffer(width, height);
		char szPath[1024] = "";
		sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
		bool bRtn = img.Load<uchar>(szPath, width, height);
		sprintf(szPath, "../data/Lena%dx%d.txt", width, height);
		float _filter[] = {
			1, 0, -1, 
			1, 0, -1,
			1, 0, -1,
		};
		std::vector<float> tmp(filter_width*filter_height);
		float* pFilter = &tmp[0];
		memcpy(pFilter, _filter, sizeof(_filter));

		float inc = FftFilter2D(img.data, dst.data, img.width, img.height, pFilter, filter_width, filter_height);

		printf("%dx%d , %dx%d ,%f ms , %f fps\n", width, height, filter_width, filter_height, inc, 1000.0 / inc);

		sprintf(szPath, "../data/cufft.%d.%d.raw", width, height);
		dst.SaveRaw(szPath);

	}
};

int main(int argc, char** argv) {
	Conv2D_FFT::Test(1024*1);
	Conv2D_FFT::Test(1024*2);
	Conv2D_FFT::Test(1024*4);	
	Conv2D_FFT::Test(1024*8);
	Conv2D_FFT::Test(9216);		
	return 0;
}