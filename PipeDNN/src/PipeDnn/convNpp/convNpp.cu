/**
 * Copyright 1993-2015 NVIDIA Corporation.  All rights reserved.
 *
 * Please refer to the NVIDIA end user license agreement (EULA) associated
 * with this source code for terms and conditions that govern your use of
 * this software. Any use, reproduction, disclosure, or distribution of
 * this software and related documentation outside the terms of the EULA
 * is strictly prohibited.
 *
 */

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#  define WINDOWS_LEAN_AND_MEAN
#  define NOMINMAX
#  include <windows.h>
#  pragma warning(disable:4819)
#endif

#ifdef _WIN32
	#pragma comment(lib, "nppif.lib")
#endif

#include <string.h>
#include <fstream>
#include <iostream>

#include <cuda_runtime.h>
#include <npp.h>
#include "../conv/common.h"
#include "../conv/cudaLib.cuh"
#ifdef _WIN32
#include "../conv/StopWatch.h"
#endif

typedef unsigned char uchar;
int TestConvNPP(int filter_width, int filter_height, int img_size=9216) {
	std::cout << filter_width << " x " << filter_height << " : ";
	typedef float DataType ;
	int RepeatCount = 3;
	int width = img_size; // 1024 * 8; // 9216; // 1024 * 8;
	int height = width;
	//const int FILTER_WIDTH = 3;
	//const int FILTER_HEIGHT = FILTER_WIDTH;
	//int filter_width = 3;
	//int filter_height = filter_width;

	float inc = 0;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
#ifdef _WIN32
	StopWatchWin watch;
#endif
	DataT<DataType> img, dst;
	img.MallocBuffer(width, height);
	char szPath[1024] = "";
	sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
	bool bRtn = false;
	bRtn = img.Load<uchar>(szPath, width, height);
	if (!bRtn) {
		img.MallocBuffer(width, height);
		std::cout << "read image failed\n";
	}
	const int offset_height = height;

	DevBuffer<DataType> devSrc(width, height + offset_height), devDst(width, devSrc.height + offset_height), devFilter(filter_width*filter_height * 2);
	
	DataT<DataType> _img, _imgDst;
	_img.MallocBuffer(devSrc.width, devSrc.height);
	_imgDst.MallocBuffer(_img.width, _img.height);

	const int offset = offset_height / 2* width;

	memcpy(_img.data + offset, img.data, img.Size()* sizeof(DataType));

	devSrc.CopyFromHost(_img.data, _img.width, _img.width, _img.height);

	NppiSize oSizeROI = { width, height };
	const Npp32f pKernel[] = {
		-1,  -1,
		1, 1,
	};
	std::vector<float> flt(filter_width*filter_height);
	memcpy(&flt[0], pKernel, sizeof(pKernel));
	
	//for (int i = 0; i < flt.size(); i++) flt[i] = 0;
	//flt[flt.size() / 2] = 1;

	NppiSize oKernelSize = {filter_width, filter_height};
	NppiPoint oAnchor = { oKernelSize.width / 2, oKernelSize.height / 2 };
	devFilter.CopyFromHost(&flt[0], flt.size(), flt.size(), 1);
	int status = (NppStatus)0;

	cudaEventRecord(start, 0);
	//watch.start();
	for (int i = 0; i < RepeatCount; i++) {
		status |= nppiFilter_32f_C1R(devSrc.GetData()+ width*height/2, devSrc.DataPitch()*sizeof(DataType) , devDst.GetData() + width*height / 2, devDst.DataPitch() * sizeof(DataType), oSizeROI,
			devFilter.GetData(), oKernelSize, oAnchor);
		cudaDeviceSynchronize();
	}
	//watch.stop();
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	//CUDA_CHECK_ERROR;

	cudaEventElapsedTime(&inc, start, stop);
	//inc = watch.getAverageTime();
	inc /= RepeatCount;
	printf("%dx%d , %dx%d ,%f ms , %f fps\n", width, height, filter_width, filter_height, inc, 1000.0 / inc);

	if (status != NPP_NO_ERROR) std::cout << "status error = " << status << std::endl;

	devDst.CopyToHost(_imgDst.data, _imgDst.width, _imgDst.width, _imgDst.height);


	dst.MallocBuffer(img.width, img.height);
	//devDst.CopyToHost(dst.data, dst.width, dst.width, dst.height);
	memcpy(dst.data, _imgDst.data + offset, img.width*img.height * sizeof(DataType));

	sprintf(szPath, "../data/npp.%d-%d.%d.%d.raw", filter_width, filter_height, width, height);
	dst.SaveRaw(szPath);

	FILE* fp = fopen("npp.csv", "at");
	if (fp) {
		fprintf(fp, "%dx%d, %dx%d, %f\n", width, height, filter_width, filter_height, inc);
		fclose(fp);
	}
	return 0;
}

int main(int argc, char** argv) {
	int img_size = 9216;
	if (argc > 1) img_size = atoi(argv[1]);
	int sz = 2;
	int cnt = 1;
	TestConvNPP(3, 3);
	//return 0;
	TestConvNPP(4, 4);
	TestConvNPP(4, 4);
	std::cout << "---------------------------------------" << std::endl;
	//TestConvNPP(2, 2);
	//sz = 2; TestConvNPP(sz, sz);
	for (int sz = 2; sz < 33; sz++) {
		for (int i = 0; i < cnt; i ++)
			TestConvNPP(sz, sz, img_size);

	}
	//sz = 3; 
	//sz = 4; TestConvNPP(sz, sz);
	//sz = 5; TestConvNPP(sz, sz);
	//sz = 6; TestConvNPP(sz, sz);
	//sz = 7; TestConvNPP(sz, sz);
	//sz = 8; TestConvNPP(sz, sz);
	//sz = 9; TestConvNPP(sz, sz);
	//sz = 10; TestConvNPP(sz, sz);
	//sz = 11; TestConvNPP(sz, sz);
	//sz = 12; TestConvNPP(sz, sz);
	//sz = 13; TestConvNPP(sz, sz);

	return 0;
}
