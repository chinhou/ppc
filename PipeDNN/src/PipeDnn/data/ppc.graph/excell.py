#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

# フォントファイルの場所を指定 ubuntu　　(C:\Windows\Fonts\任意のFont)
#fp = FontProperties(fname="/usr/share/fonts/truetype/fonts-japanese-gothic.ttf")

####################################################################

filepath='E:/8192x8192.V100.xlsx' #Xlsファイルの場所

df=pd.read_excel(filepath, parse_cols="B:F") #Pandas DataFrameに読込み

data_range= range(0, 15)

#列で指定
data_ours=df['ours'][data_range]
data_arrayfire=df['ArrayFire'][data_range]
data_NPP=df['NPP'][data_range]

x = range(1, len(data_ours)+1)

xticks = []
for i in x:
    s = "%dx%d" % (i+1, i+1)
    xticks.append(s)

fig = plt.figure()
plt.rcParams["font.size"] = 18
plt.grid(True)
plt.plot(x, data_ours, marker="x", label="ours")
plt.plot(x, data_arrayfire, marker="o", label="Arrayfire")
plt.plot(x, data_NPP, marker="v", label="NPP")

plt.legend(loc="upper left")
plt.title("8192x8192 convolution")
plt.xlabel("Filter size")
plt.ylabel("Execute time (ms)")

plt.xticks(rotation=-60)
plt.xticks(x, xticks)

plt.tight_layout()
plt.savefig("sample.pdf")
plt.show()