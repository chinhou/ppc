#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
import os.path


data_names_list = ["our", "ArrayFire", "NPP", "cuDNN", "cuFFT-based"]
#data_names = ["ours", "ArrayFire", "NPP",]

def draw(files, output_name, data_names = "ours"):
    data_rows = 20

    fig = plt.figure()
    plt.grid(True)
    x = range(1, data_rows+1)
    xticks_lable = []
    for i in x:
        s = "%dx%d" % (i+1, i+1)
        xticks_lable.append(s)

    def get_data(filepath, col_name, size):
        df = pd.read_excel(filepath, parse_cols="B:I")  # Pandas DataFrameに読込み
        return df[col_name][0:size]

    for s, file in enumerate(files):
        y = get_data(file[0], data_names, len(x))
        plt.plot(x, y, label=file[1])

    plt.legend(loc="upper left")
    plt.title("convolution limitations")
    plt.xlabel("Filter size")
    plt.ylabel("Execute time (ms)")
    plt.xticks(rotation=-90)
    plt.xticks(x, xticks_lable)

    plt.tight_layout()
    plt.savefig(output_name)
    plt.show()
    plt.close()

def v100():
    folder = 'E:/VirtualBoxData/data/V100'
    name_idx = [0, 1, 2]
    files_list = [
        [folder + '/1024x1024.V100.xlsx', "1024x1024 convolution"],
        [folder + '/2048x2048.V100.xlsx', "2048x2048 convolution"],
        [folder + '/4096x4096.V100.xlsx', "4096x4096 convolution"],
        [folder + '/8192x8192.V100.xlsx', "8192x8192 convolution"],
    ]
    output_name = "./mygraph/V100.limitation.pdf"
    draw(files_list, output_name)


def TitanXp():
    folder = 'E:\VirtualBoxData\data\TitanXp\TitanXpWIndows'
    name_idx = [0, 1]
    files_list = [
        [folder + '/1024x1024.TitanXp.xlsx', "1024x1024 convolution"],
        [folder + '/2048x2048.TitanXp.xlsx', "2048x2048 convolution"],
        [folder + '/4096x4096.TitanXp.xlsx', "4096x4096 convolution"],
        [folder + '/8192x8192.TitanXp.xlsx', "8192x8192 convolution"],
    ]
    output_name =  "./mygraph/TitanXp.limitation.pdf"
    draw(files_list, output_name, "our")

def main():
    if os.path.exists('./mygraph') == False:
        os.mkdir("mygraph")
    #v100()
    TitanXp()




if __name__ == '__main__':
    main()

