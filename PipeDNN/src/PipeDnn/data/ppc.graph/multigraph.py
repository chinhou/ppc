#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os


data_names_list = ["our", "ArrayFire", "NPP", "cuDNN", "cuFFT-based"]
#data_names = ["ours", "ArrayFire", "NPP",]

def draw(name_idx, files, output_name):
    data_names = []
    for i in name_idx:
        data_names.append(data_names_list[name_idx[i]])

    data_rows = 15

    #fig = plt.figure()
    x = range(1, data_rows+1)
    xticks_lable = []
    for i in x:
        s = "%dx%d" % (i+1, i+1)
        xticks_lable.append(s)

    def get_data(filepath, col_name, size):
        df = pd.read_excel(filepath, parse_cols="B:I")  # Pandas DataFrameに読込み
        return df[col_name][0:size]

    #subfigs = [fig.add_subplot(121), fig.add_subplot(122)]

    fig, subfigs = plt.subplots(nrows=1, ncols=len(files), figsize=(15,3))

    plt.setp(subfigs, xticks=x, xticklabels=xticks_lable)


    for idx, subfig in enumerate(subfigs):

        _subfig = subfigs[idx]
        for s, name in enumerate(data_names):
            y = get_data(files[idx][0], data_names[s], len(x))
            _subfig.plot(x, y, label=data_names[s])

        #_subfig.legend(loc="upper left")
        #_subfig.title(files[idx][1])
        _subfig.title.set_text(files[idx][1])
        _subfig.set_xlabel("Filter size")
        _subfig.set_ylabel("Execute time (ms)")
        _subfig.set_xticks(x, xticks_lable)
        _subfig.grid(True)
        _subfig.legend()

    for ax in fig.axes:
        plt.sca(ax)
        plt.xticks(rotation=-90)
        pass

    plt.tight_layout()

    plt.savefig(output_name)
    plt.show()
    plt.close()

def v100():
    folder = 'E:/VirtualBoxData/data/V100'
    name_idx = [0, 1, 2]
    files_list = [
        [folder + '/1024x1024.V100.xlsx', "1024x1024 convolution"],
        [folder + '/2048x2048.V100.xlsx', "2048x2048 convolution"],
        [folder + '/4096x4096.V100.xlsx', "4096x4096 convolution"],
        [folder + '/8192x8192.V100.xlsx', "8192x8192 convolution"],
    ]
    output_name = "./mygraph/V100.all.pdf"
    draw(name_idx, files_list, output_name)


def TitanXp():
    folder = 'E:\VirtualBoxData\data\TitanXp\TitanXpWIndows'
    name_idx = [0, 1]
    files_list = [
        [folder + '/1024x1024.TitanXp.xlsx', "1024x1024 convolution"],
        [folder + '/2048x2048.TitanXp.xlsx', "2048x2048 convolution"],
        [folder + '/4096x4096.TitanXp.xlsx', "4096x4096 convolution"],
        [folder + '/8192x8192.TitanXp.xlsx', "8192x8192 convolution"],
    ]
    output_name = "./mygraph/TitanXp.ours.arrayfire.pdf"
    draw(name_idx, files_list, output_name)

def main():
    if os.path.exists('./mygraph') == False:
        os.mkdir("mygraph")
    #v100()
    TitanXp()




if __name__ == '__main__':
    main()

