#ifdef _WIN32
#include <Windows.h>
#endif

#include <stdio.h>
#include "../conv/common.h"
#include <iostream>

#include "cuda.h"
#include <cudnn.h>

#include "error_util.h"
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

#pragma comment(lib, "cudnn.lib")

using namespace std;

int main() {
	const int minibatch_size = 1;
	const int feature_num = 1;
	const int filter_num = 1;
	const int in_size = 1024*8;
	const int filter_size = 3;

	// 入力
	static float srcData[minibatch_size][feature_num][in_size][in_size] = {
		{
			{ { 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 1, 0 },{ 1, 1, 0, 0, 0, 0, 0, 1 },{ 0, 1, 0, 1, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 } },
		}
	};

	float* p = &srcData[0][0][0][0];
	ImageT<float> img;
	char szPath[1024];
	sprintf(szPath, "../data/Lena%dx%d.raw", in_size, in_size);
	img.Load<UCHAR>(szPath, in_size, in_size);

#if 0
	for (int i = 0; i < sizeof(srcData) / sizeof(float); i++)
		p[i] = 1;
#else
	memcpy(p, img.data, sizeof(srcData));
#endif
	float *srcData_dev;
	checkCudaErrors(cudaMalloc((void**)&srcData_dev, sizeof(srcData)));
	// Copy input vectors from host memory to GPU buffers.
	checkCudaErrors(cudaMemcpy(srcData_dev, srcData, sizeof(srcData), cudaMemcpyHostToDevice));

	// 出力
	static float dstData[minibatch_size][filter_num][in_size][in_size];
	float *dstData_dev;
	checkCudaErrors(cudaMalloc((void**)&dstData_dev, sizeof(dstData)));
#if 0
	// フィルター係数
	static float filterData[filter_num][feature_num][filter_size][filter_size] = {
		{
			{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
			{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
		},
		{
			{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
			{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
		},
		{
			{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
			{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
		}
	};
	// バイアス
	float biasData[filter_num] = {
		0.0f, 0.0f, 0.0f
	};
#else
	// フィルター係数
	static float filterData[filter_num][feature_num][filter_size][filter_size] = {
		{
			{ 
				{ 1.0,  2.0,    1.0, },
				{ 0,    0.0,      0, },
				{ -1.0,  -2.0, -1.0, },
			},
		},
	};
	float biasData[filter_num] = {
		0.0f,
	};
#endif
	float *filterData_dev;
	checkCudaErrors(cudaMalloc((void**)&filterData_dev, sizeof(filterData)));
	// Copy input vectors from host memory to GPU buffers.
	checkCudaErrors(cudaMemcpy(filterData_dev, filterData, sizeof(filterData), cudaMemcpyHostToDevice));


	float *biasData_dev;
	checkCudaErrors(cudaMalloc((void**)&biasData_dev, sizeof(biasData)));
	// Copy input vectors from host memory to GPU buffers.
	checkCudaErrors(cudaMemcpy(biasData_dev, biasData, sizeof(biasData), cudaMemcpyHostToDevice));


	// 畳み込み準備
	cudnnHandle_t cudnnHandle;
	cudnnTensorDescriptor_t srcTensorDesc, dstTensorDesc, biasTensorDesc;
	cudnnFilterDescriptor_t filterDesc;
	cudnnConvolutionDescriptor_t convDesc;
	//cudnnCreate(&cudnnHandle);
	checkCUDNN(cudnnCreate(&cudnnHandle));
	checkCUDNN(cudnnCreateTensorDescriptor(&srcTensorDesc));
	checkCUDNN(cudnnCreateTensorDescriptor(&dstTensorDesc));
	checkCUDNN(cudnnCreateTensorDescriptor(&biasTensorDesc));
	checkCUDNN(cudnnCreateFilterDescriptor(&filterDesc));
	checkCUDNN(cudnnCreateConvolutionDescriptor(&convDesc));

	checkCUDNN(cudnnSetTensor4dDescriptor(srcTensorDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, minibatch_size, feature_num, in_size, in_size));
	checkCUDNN(cudnnSetTensor4dDescriptor(dstTensorDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, minibatch_size, filter_num, in_size, in_size));
	checkCUDNN(cudnnSetTensor4dDescriptor(biasTensorDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, 1, filter_num, 1, 1));
	checkCUDNN(cudnnSetFilter4dDescriptor(filterDesc, CUDNN_DATA_FLOAT, CUDNN_TENSOR_NCHW, filter_num, feature_num, filter_size, filter_size));
	checkCUDNN(cudnnSetConvolution2dDescriptor(convDesc, filter_size/2/*pad_h*/, filter_size/2/*pad_w*/, 1/*stride_h*/, 1/*stride_w*/, 1, 1, CUDNN_CROSS_CORRELATION));

	cudnnConvolutionFwdAlgo_t algo = cudnnConvolutionFwdAlgo_t(0);
	checkCUDNN(cudnnGetConvolutionForwardAlgorithm(cudnnHandle,
		srcTensorDesc,
		filterDesc,
		convDesc,
		dstTensorDesc,
		CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
		0,
		&algo
	));

	cout << "Fastest algorithm is Algo " << algo << endl;

	size_t sizeInBytes = 0;
	checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnnHandle,
		srcTensorDesc,
		filterDesc,
		convDesc,
		dstTensorDesc,
		algo,
		&sizeInBytes));
	cout << "workSpace " << sizeInBytes << endl;

	void* workSpace = NULL;
	if (sizeInBytes != 0)
	{
		checkCudaErrors(cudaMalloc(&workSpace, sizeInBytes));
	}

	float inc = 0;
	cudaEvent_t start, stop;
	checkCudaErrors(cudaEventCreate(&start));
	checkCudaErrors(cudaEventCreate(&stop));

	cudaEventRecord(start, 0);
	//float ftm = timeGetTime();
	// 畳み込み
	float alpha = 1.0f;
	float beta = 0.0f;
	//int nTimes = 100;
	//for (int i = 0; i < nTimes; i++)
	checkCUDNN(cudnnConvolutionForward(cudnnHandle,
		&alpha,
		srcTensorDesc,
		srcData_dev,
		filterDesc,
		filterData_dev,
		convDesc,
		algo,
		workSpace,
		sizeInBytes,
		&beta,
		dstTensorDesc,
		dstData_dev));

	checkCudaErrors(cudaDeviceSynchronize());

	checkCudaErrors(cudaEventRecord(stop, 0));
	cudaEventSynchronize(stop);

	//ftm = timeGetTime() - ftm;
	// バイアス
	alpha = 1.0f;
	beta = 1.0f;
	//checkCUDNN(cudnnAddTensor(cudnnHandle, &alpha, biasTensorDesc, biasData_dev, &beta, dstTensorDesc, dstData_dev));
	float ftm = timeGetTime();
	// 出力表示
	// Copy output vector from GPU buffer to host memory.
	checkCudaErrors(cudaMemcpy(dstData, dstData_dev, sizeof(dstData), cudaMemcpyDeviceToHost));
	//checkCudaErrors(cudaDeviceSynchronize());
	ftm = timeGetTime() - ftm;

	//checkCudaErrors(cudaEventRecord(stop, 0));

	checkCudaErrors(cudaEventElapsedTime(&inc, start, stop));

	printf("%d x %d , %f ms , %f fps\n", in_size, in_size, inc, (1000.0 / inc));
	//printf("%d times, %d x %d , %f ms ,\n", nTimes, in_size, in_size, ftm);
	printf("%d x %d , %f ms ,\n", in_size, in_size, ftm);

#if 1
	memcpy(img.data, &dstData[0][0][0][0], sizeof(srcData));
	sprintf(szPath, "../data/Lena.cudnn.%dx%d.raw", in_size, in_size);
	img.Save(szPath);
#endif
/*
	for (int i = 0; i < filter_num; i++) {
		for (int y = 0; y < in_size; y++) {
			cout << "{";
			for (int x = 0; x < in_size; x++) {
				cout << dstData[0][i][y][x] << ", ";
			}
			cout << "}, ";
		}
		cout << endl;
	}
*/
	FILE* fp = fopen("cuDNN.csv", "at");
	if (fp) {
		fprintf(fp, "%d x %d , %f ms \n", in_size, in_size, inc);
		fclose(fp);
	}
	checkCUDNN(cudnnDestroyConvolutionDescriptor(convDesc));
	checkCUDNN(cudnnDestroyFilterDescriptor(filterDesc));
	checkCUDNN(cudnnDestroyTensorDescriptor(srcTensorDesc));
	checkCUDNN(cudnnDestroyTensorDescriptor(dstTensorDesc));
	checkCUDNN(cudnnDestroyTensorDescriptor(biasTensorDesc));
	checkCUDNN(cudnnDestroy(cudnnHandle));

	return 0;
}