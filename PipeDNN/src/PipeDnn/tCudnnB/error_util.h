#ifndef __ERROR_UIL_H
#define __ERROR_UIL_H

#pragma once
#include <assert.h>

#define CUDNN      7
//#define CUDNN5 1

#ifdef _WIN32
#if CUDNN == 5 
#include "./cudnn-8.0-windows10-x64-v5.0-ga/cuda/include/cudnn.h"
#pragma comment(lib, "./cudnn-8.0-windows10-x64-v5.0-ga/cuda/lib/x64/cudnn.lib")
#endif

#if CUDNN == 6 
#include "./cudnn-8.0-windows10-x64-v6.0/cuda/include/cudnn.h"
#pragma comment(lib, "./cudnn-8.0-windows10-x64-v6.0/cuda/lib/x64/cudnn.lib")
#endif

#if CUDNN == 7 
#include "./cudnn-9.0-windows10-x64-v7/cuda/include/cudnn.h"
#pragma comment(lib, "./cudnn-9.0-windows10-x64-v7/cuda/lib/x64/cudnn.lib")
#endif

#else
static int timeGetTime() {
	return 0;
}
#include "cudnn.h"
#endif

#ifdef __DRIVER_TYPES_H__
#ifndef DEVICE_RESET
#define DEVICE_RESET cudaDeviceReset();
#endif
#else
#ifndef DEVICE_RESET
#define DEVICE_RESET
#endif
#endif

static const char *_cudaGetErrorEnum(int error)
{
	switch (error)
	{
	case CUDNN_STATUS_SUCCESS:
		return "CUDNN_STATUS_SUCCESS";
	default:
		break;
	}


	return "<unknown>";
}

template< typename T >
void check(T result, char const *const func, const char *const file, int const line)
{
	if (result)
	{
		fprintf(stderr, "CUDA error at %s:%d code=%d(%s) \"%s\" \n",
			file, line, static_cast<unsigned int>(result), _cudaGetErrorEnum(result), func);
		DEVICE_RESET
			// Make sure we call CUDA Device Reset before exiting
			exit(EXIT_FAILURE);
	}
}

#ifdef __DRIVER_TYPES_H__
// This will output the proper CUDA error strings in the event that a CUDA host call returns an error
#define checkCudaErrors(val)           check ( (val), #val, __FILE__, __LINE__ )

// This will output the proper error string when calling cudaGetLastError
#define getLastCudaError(msg)      __getLastCudaError (msg, __FILE__, __LINE__)

inline void __getLastCudaError(const char *errorMessage, const char *file, const int line)
{
	cudaError_t err = cudaGetLastError();

	if (cudaSuccess != err)
	{
		fprintf(stderr, "%s(%i) : getLastCudaError() CUDA error : %s : (%d) %s.\n",
			file, line, errorMessage, (int)err, cudaGetErrorString(err));
		DEVICE_RESET
			exit(EXIT_FAILURE);
	}
}

// This will only print the proper error string when calling cudaGetLastError but not exit program incase error detected.
#define printLastCudaError(msg)      __printLastCudaError (msg, __FILE__, __LINE__)

inline void __printLastCudaError(const char *errorMessage, const char *file, const int line)
{
	cudaError_t err = cudaGetLastError();

	if (cudaSuccess != err)
	{
		fprintf(stderr, "%s(%i) : getLastCudaError() CUDA error : %s : (%d) %s.\n",
			file, line, errorMessage, (int)err, cudaGetErrorString(err));
	}
}
#endif

#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif

#define checkCUDNN(_x)  check ( (_x), #_x, __FILE__, __LINE__ )


















#endif
