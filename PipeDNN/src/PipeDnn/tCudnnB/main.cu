#ifdef _WIN32
#include <Windows.h>
#endif

#include <stdio.h>
#include "../conv/common.h"
#include <iostream>

#include "cuda.h"

#include "error_util.h"
#ifdef _WIN32
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
#endif

//struct cuDnnForwardFilter {
//	cuDnnForwardFilter() {
//	}
//
//
//	int data_width, data_heith, data_channels, data_number;
//	int filter_width, filter_height, filter_channels, filter_number;
//	int pad_w, pad_h, stride_w, stride_h;
//};

using namespace std;

int run_cudnn(int data_size, int channels, int filter_size, int filter_num) {
	const int minibatch_size = 1;
	//const int channels = 3;
	//const int filter_num = 32;
	//const int data_size = 32;
	//const int filter_size = 3;



	const int feature_num = channels;
	const int in_size = data_size;

	DataT<float> src_buf(in_size, in_size, feature_num, minibatch_size);
	float* srcData = src_buf.data;
	// 入力
	//static float srcData[minibatch_size][feature_num][in_size][in_size] = {
	//	{
	//		{ { 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 1, 0 },{ 1, 1, 0, 0, 0, 0, 0, 1 },{ 0, 1, 0, 1, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 },{ 0, 1, 0, 0, 0, 0, 0, 0 } },
	//	}
	//};

	float* p = &srcData[0];
	ImageT<float> img;
	char szPath[1024];
	sprintf(szPath, "../data/Lena%dx%d.raw", in_size, in_size);
	//img.Load<UCHAR>(szPath, in_size, in_size);

#if 1
	for (int i = 0; i < src_buf.Size(); i++)
		p[i] = 1;
#else
	memcpy(p, img.data, sizeof(srcData));
#endif
	float *srcData_dev;
	checkCudaErrors(cudaMalloc((void**)&srcData_dev, sizeof(float)*src_buf.Size()));
	// Copy input vectors from host memory to GPU buffers.
	checkCudaErrors(cudaMemcpy(srcData_dev, srcData, sizeof(float)*src_buf.Size(), cudaMemcpyHostToDevice));

	DataT<float> dst_buf(in_size, in_size, filter_num, minibatch_size);
	float* dstData = dst_buf.data;
	// 出力
	//static float dstData[minibatch_size][filter_num][in_size][in_size];
	float *dstData_dev;
	checkCudaErrors(cudaMalloc((void**)&dstData_dev, sizeof(float)*dst_buf.Size()));
#if 0
	// フィルター係数
	static float filterData[filter_num][feature_num][filter_size][filter_size] = {
		{
			{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
			{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
		},
		{
			{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
			{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
		},
		{
			{ { 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f },{ 0.1f, 1.0f, 0.1f, 0.5f, 0.2f } },
			{ { 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f },{ 1.0f, 0.1f, 0.1f, 0.5f, 1.0f } }
		}
	};
	// バイアス
	float biasData[filter_num] = {
		0.0f, 0.0f, 0.0f
	};
#else
	DataT<float> flt_buf(filter_size, filter_size, feature_num, filter_num);
	for (int i = 0; i < flt_buf.Size(); i++) flt_buf.data[i] = 1;

	float* filterData = flt_buf.data;
	//// フィルター係数
	//static float filterData[filter_num][feature_num][filter_size][filter_size] = {
	//	{
	//		{
	//			{ 1.0,  2.0,    1.0, },
	//			{ 0,    0.0,      0, },
	//			{ -1.0,  -2.0, -1.0, },
	//		},
	//	},
	//};
	//float biasData[filter_num] = {
	//	0.0f,
	//};
	//float* pF = &filterData[0][0][0][0];
	//for (int i = 0; i < sizeof(filterData) / sizeof(float); i++)
	//	pF[i] = 1;
#endif
	float *filterData_dev;
	checkCudaErrors(cudaMalloc((void**)&filterData_dev, sizeof(float)*flt_buf.Size()));
	// Copy input vectors from host memory to GPU buffers.
	checkCudaErrors(cudaMemcpy(filterData_dev, filterData, sizeof(float)*flt_buf.Size(), cudaMemcpyHostToDevice));


	//float *biasData_dev;
	//checkCudaErrors(cudaMalloc((void**)&biasData_dev, sizeof(biasData)));
	// Copy input vectors from host memory to GPU buffers.
	//checkCudaErrors(cudaMemcpy(biasData_dev, biasData, sizeof(biasData), cudaMemcpyHostToDevice));


	// 畳み込み準備
	cudnnHandle_t cudnnHandle;
	cudnnTensorDescriptor_t srcTensorDesc, dstTensorDesc, biasTensorDesc;
	cudnnFilterDescriptor_t filterDesc;
	cudnnConvolutionDescriptor_t convDesc;
	//cudnnCreate(&cudnnHandle);
	checkCUDNN(cudnnCreate(&cudnnHandle));
	checkCUDNN(cudnnCreateTensorDescriptor(&srcTensorDesc));
	checkCUDNN(cudnnCreateTensorDescriptor(&dstTensorDesc));
	checkCUDNN(cudnnCreateTensorDescriptor(&biasTensorDesc));
	checkCUDNN(cudnnCreateFilterDescriptor(&filterDesc));
	checkCUDNN(cudnnCreateConvolutionDescriptor(&convDesc));

	checkCUDNN(cudnnSetTensor4dDescriptor(srcTensorDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, minibatch_size, feature_num, in_size, in_size));
	checkCUDNN(cudnnSetTensor4dDescriptor(dstTensorDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, minibatch_size, filter_num, in_size, in_size));
	checkCUDNN(cudnnSetTensor4dDescriptor(biasTensorDesc, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, 1, filter_num, 1, 1));
	checkCUDNN(cudnnSetFilter4dDescriptor(filterDesc, CUDNN_DATA_FLOAT, CUDNN_TENSOR_NCHW, filter_num, feature_num, filter_size, filter_size));

#if CUDNN <= 5
	checkCUDNN(cudnnSetConvolution2dDescriptor(convDesc, filter_size / 2/*pad_h*/, filter_size / 2/*pad_w*/, 1/*stride_h*/, 1/*stride_w*/, 1, 1, CUDNN_CROSS_CORRELATION));
#endif

#if CUDNN >= 6 
	checkCUDNN(cudnnSetConvolution2dDescriptor(convDesc, filter_size / 2/*pad_h*/, filter_size / 2/*pad_w*/, 1/*stride_h*/, 1/*stride_w*/, 1, 1, CUDNN_CROSS_CORRELATION, CUDNN_DATA_FLOAT));
#endif


	cudnnConvolutionFwdAlgo_t algo = cudnnConvolutionFwdAlgo_t(8);
	checkCUDNN(cudnnGetConvolutionForwardAlgorithm(cudnnHandle,
		srcTensorDesc,
		filterDesc,
		convDesc,
		dstTensorDesc,
		CUDNN_CONVOLUTION_FWD_PREFER_FASTEST,
		0,
		&algo
	));
	//algo = cudnnConvolutionFwdAlgo_t(0);

	cout << "Fastest algorithm is Algo " << algo << endl;

	size_t sizeInBytes = 0;
	checkCUDNN(cudnnGetConvolutionForwardWorkspaceSize(cudnnHandle,
		srcTensorDesc,
		filterDesc,
		convDesc,
		dstTensorDesc,
		algo,
		&sizeInBytes));
	cout << "workSpace " << sizeInBytes << endl;

	void* workSpace = NULL;
	if (sizeInBytes != 0)
	{
		checkCudaErrors(cudaMalloc(&workSpace, sizeInBytes));
	}

	float inc = 0;
	cudaEvent_t start, stop;
	checkCudaErrors(cudaEventCreate(&start));
	checkCudaErrors(cudaEventCreate(&stop));

	cudaEventRecord(start, 0);
	//float ftm = timeGetTime();
	// 畳み込み
	float alpha = 1.0f;
	float beta = 0.0f;
	//int nTimes = 100;
	//for (int i = 0; i < nTimes; i++)
	checkCUDNN(cudnnConvolutionForward(cudnnHandle,
		&alpha,
		srcTensorDesc,
		srcData_dev,
		filterDesc,
		filterData_dev,
		convDesc,
		algo,
		workSpace,
		sizeInBytes,
		&beta,
		dstTensorDesc,
		dstData_dev));

	checkCudaErrors(cudaDeviceSynchronize());

	checkCudaErrors(cudaEventRecord(stop, 0));
	cudaEventSynchronize(stop);

	//ftm = timeGetTime() - ftm;
	// バイアス
	alpha = 1.0f;
	beta = 1.0f;
	//checkCUDNN(cudnnAddTensor(cudnnHandle, &alpha, biasTensorDesc, biasData_dev, &beta, dstTensorDesc, dstData_dev));
	float ftm = timeGetTime();
	// 出力表示
	// Copy output vector from GPU buffer to host memory.
	checkCudaErrors(cudaMemcpy(dstData, dstData_dev, sizeof(float)*dst_buf.Size(), cudaMemcpyDeviceToHost));
	//checkCudaErrors(cudaDeviceSynchronize());
	ftm = timeGetTime() - ftm;

	//checkCudaErrors(cudaEventRecord(stop, 0));

	checkCudaErrors(cudaEventElapsedTime(&inc, start, stop));

	printf("(%d x %d x %d), (%d x %d x %d x %d), %f ms , %f fps\n", in_size, in_size, feature_num, filter_size, filter_size, feature_num, filter_num
		,inc, (1000.0 / inc));
	//printf("%d times, %d x %d , %f ms ,\n", nTimes, in_size, in_size, ftm);
	//printf("%d x %d , %f ms ,\n", in_size, in_size, ftm);

#if 1
	DataT<float> dst;
	dst.MallocBuffer(in_size, in_size, filter_num, 1);
	memcpy(dst.data, &dstData[0], sizeof(float)*dst_buf.Size());
	sprintf(szPath, "../data/tCudnnB.%dx%dx%dx%d.txt", in_size, in_size, filter_num, 1);
	//dst.SaveText(szPath);
	//sprintf(szPath, "../data/Lena.cudnn.%dx%d.raw", in_size, in_size);
	//img.Save(szPath);
#endif

	char szCSV[1024];
	sprintf(szCSV, "cudnn%d.csv", algo);
	FILE* fp = fopen(szCSV, "at");
	if (fp) {
		fprintf(fp, "%dx%d, %dx%d, %f, %d\n", in_size, in_size, filter_size, filter_size, inc, sizeInBytes);
		fclose(fp);
	}

	/*
	for (int i = 0; i < filter_num; i++) {
	for (int y = 0; y < in_size; y++) {
	cout << "{";
	for (int x = 0; x < in_size; x++) {
	cout << dstData[0][i][y][x] << ", ";
	}
	cout << "}, ";
	}
	cout << endl;
	}
	*/
	checkCUDNN(cudnnDestroyConvolutionDescriptor(convDesc));
	checkCUDNN(cudnnDestroyFilterDescriptor(filterDesc));
	checkCUDNN(cudnnDestroyTensorDescriptor(srcTensorDesc));
	checkCUDNN(cudnnDestroyTensorDescriptor(dstTensorDesc));
	checkCUDNN(cudnnDestroyTensorDescriptor(biasTensorDesc));
	checkCUDNN(cudnnDestroy(cudnnHandle));

	return 0;
}

int main(int argc, char** argv) {
#if 1
	int data_size = atoi(argv[1]);
	int channels = atoi(argv[2]);
	int filter_size = atoi(argv[3]);
	int filter_num = atoi(argv[4]);
#else
	int data_size = 514;
	int channels = 3;
	int filter_size = 3;
	int filter_num = 32;
#endif
	run_cudnn(data_size, channels, filter_size, filter_num);

	return 0;
}

