#include "cudaLib.cuh"
#include "common.h"
#ifdef _WIN32
#include "StopWatch.h"
#endif

template<typename T> __device__ __forceinline__ T Mad(T a, T b, T c);

template<> __device__ __forceinline__ int Mad<int>(int a, int b, int c) {
	asm("mad.wide.s32 %0, %1, %2, %3;" : "=r"(c) : "r"(a), "r"(b), "r"(c));
	return c;
}

template<> __device__ __forceinline__  uint Mad<uint>(uint a, uint b, uint c) {
	asm("mad.wide.u32 %0, %1, %2, %3;" : "=r"(c) : "r"(a), "r"(b), "r"(c));
	return c;
}

template<> __device__ __forceinline__  float Mad<float>(float a, float b, float c) {
	asm("mad.rn.f32 %0, %1, %2, %3;" : "=f"(c) : "f"(a), "f"(b), "f"(c));
	return c;
}

template<typename T, int REG_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT>
__global__ void kerenlPipeConv(const T* __restrict__ src, T* dst, int width, int widthStride, int height, 
	const T* __restrict__ weight, int wx, int wy) {
	T data[REG_COUNT];
	int tidx = threadIdx.x + blockDim.x*blockIdx.x;
	int tidy = threadIdx.y + blockDim.y*blockIdx.y;
	int laneId = threadIdx.x & 31;
	int warpId = threadIdx.x >> 5;
	uint warpCount = blockDim.x >> 5;

	__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
	T* psmem = &smem[0][0];
	if (tidx < FILTER_HEIGHT*FILTER_WIDTH)
		psmem[tidx] = weight[tidx];
	__syncthreads();

	int index = widthStride*tidy*(REG_COUNT-1) + tidx;
	#pragma unroll
	for (int s = 0; s < REG_COUNT; s++) {
		if (s + tidy*REG_COUNT < height) {
			data[s] = src[index];
			index += widthStride;
		}
	}
	index = widthStride*tidy*(REG_COUNT-1) + tidx;
	#pragma unroll
	for (int i = 0; i < REG_COUNT - FILTER_HEIGHT + 1; i ++) {
		T sum = 0;
		#pragma unroll
		for (int m = 0; m < FILTER_WIDTH; m++) {
			if (m > 0)
				sum = __shfl_up(sum, 1);
			#pragma unroll
			for (int n = 0; n < FILTER_HEIGHT; n++) {
				//sum += data[i + n] * smem[n][m];
				sum = Mad(data[i + n], smem[n][m], sum);
			}
		}
		if (i + tidy < height) {
			dst[index] = sum;
			index += widthStride;
		}
	}
}

template<typename T, int REG_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT>
__global__ void kerenlPipeConvCycle(const T* __restrict__ src, T* dst, int width, int widthStride, int height,
	const T* __restrict__ weight, int wx, int wy) {
	T data[FILTER_HEIGHT];
	int tidx = threadIdx.x + blockDim.x*blockIdx.x;
	int tidy = threadIdx.y + blockDim.y*blockIdx.y;
	int laneId = threadIdx.x & 31;
	int warpId = threadIdx.x >> 5;
	uint warpCount = blockDim.x >> 5;

	__shared__ T smem[FILTER_HEIGHT][FILTER_WIDTH];
	T* psmem = &smem[0][0];
	if (tidx < FILTER_HEIGHT*FILTER_WIDTH)
		psmem[tidx] = weight[tidx];
	__syncthreads();

	int row_index = 0;
	int index = widthStride*tidy*(REG_COUNT - 1) + tidx;
#pragma unroll
	for (int s = 0; s < FILTER_HEIGHT; s++) {
		if (s + tidy*REG_COUNT < height) {
			data[s] = src[index];
			index += widthStride;
		}
	}
	index = widthStride*tidy*(REG_COUNT - 1) + tidx;
#pragma unroll
	for (int i = 0; i < REG_COUNT - FILTER_HEIGHT + 1; i++) {
		T sum = 0;
#pragma unroll
		for (int m = 0; m < FILTER_WIDTH; m++) {
			if (m > 0)
				sum = __shfl_up(sum, 1);
#pragma unroll
			for (int n = 0; n < FILTER_HEIGHT; n++) {
				//sum += data[i + n] * smem[n][m];
				sum = Mad(data[i + n], smem[n][m], sum);
			}
		}
		if (i + tidy < height) {
			dst[index] = sum;
			index += widthStride;
		}
	}
}


void runPipeConv(int width, int height) {
	//int width = 1024*4;
	//int height = 1024*4;
	typedef float DataType;

	float inc = 0;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	//StopWatchWin watch;
	ImageT<DataType> img;
	char szPath[1024] = "";
	sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
	bool bRtn = img.Load<uchar>(szPath, width, height);
	if (!bRtn) printf("Load failed : %s\n", szPath);
	DevData<DataType> devSrc(width, height), devDst(width, height);
	devSrc.CopyFromHost(img.data, img.width, img.width, img.height);
	ImageT<DataType> imgDst;
	imgDst.MallocBuffer(width, height);

	const int BLOCK_SIZE = 48;
	dim3 block_size(1024, 1);
	dim3 grid_size(UpDivide(width, block_size.x), UpDivide(height, BLOCK_SIZE));

	const int FILTER_WIDTH = 3;
	const int FILTER_HEIGHT = 3;
	//DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
	//	{ 1.0,  1.0, 1.0,},
	//	{ 1.0, -8.0, 1.0,},
	//	{ 1.0,  1.0, 1.0,},
	//};
	DataType filter[FILTER_HEIGHT][FILTER_WIDTH] = {
		{ 1.0,  2.0, 1.0, },
		{ 0,  0.0, 0, },
		{ -1.0,  -2.0, -1.0, },
	};

	DevData<DataType> devFilter(FILTER_WIDTH*FILTER_WIDTH);
	devFilter.CopyFromHost(&filter[0][0], FILTER_WIDTH*FILTER_HEIGHT, FILTER_WIDTH*FILTER_HEIGHT, 1);
	
	cudaEventRecord(start, 0);
	//watch.start();
	kerenlPipeConv<DataType, BLOCK_SIZE, FILTER_WIDTH, FILTER_HEIGHT> <<<grid_size, block_size >>> (devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, devFilter.GetData(), FILTER_WIDTH, FILTER_HEIGHT);
	cudaDeviceSynchronize();
	//watch.stop();
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);

	devDst.CopyToHost(imgDst.data, imgDst.width, imgDst.width, imgDst.height);

	cudaEventElapsedTime(&inc, start, stop);
	//inc = watch.getAverageTime();
	printf("%dx%d , %f ms , %f fps\n", width, height, inc, 1000.0/inc);
	sprintf(szPath, "../data/Lena_proc_%dx%d.raw", width, height);
	imgDst.Save(szPath);
}

void Test() {
	for (int i=1; i<9; i ++)
		runPipeConv(i*1024, i*1024);
}
