#include "cudaLib.cuh"
#include "common.h"
#ifdef _WIN32
#include "StopWatch.h"
#endif
//#include "cuda_common.cuh"

static const int WARP_SIZE = 32;

template<typename T> __device__ __forceinline__ T MAD(T a, T b, T c);

template<> __device__ __forceinline__ int MAD<int>(int a, int b, int c) {
	asm("mad.wide.s32 %0, %1, %2, %3;" : "=r"(c) : "r"(a), "r"(b), "r"(c));
	return c;
}

template<> __device__ __forceinline__  uint MAD<uint>(uint a, uint b, uint c) {
	asm("mad.wide.u32 %0, %1, %2, %3;" : "=r"(c) : "r"(a), "r"(b), "r"(c));
	return c;
}

template<> __device__ __forceinline__  float MAD<float>(float a, float b, float c) {
	float d;
	asm volatile("mad.rz.ftz.f32 %0,%1,%2,%3;" : "=f"(d) : "f"(a), "f"(b), "f"(c));
	return d;
}


template<typename T, int PROCESS_DATA_COUNT, int PROCESS_FILTER_COUNT, int FILTER_WIDTH, int FILTER_HEIGHT, int FILTER_CHANNELS, int FILTER_NUMBER, int BLOCK_DIM_X>
__global__ void kerenlOneBlockOneChannel(int BLOCK_NUM_X, const T* __restrict__ src, T* dst, int width, int widthStride, int height, int channels,
	const T* __restrict__ filter, int fx, int fy, int fc, int fn, int padw, int padh)
{
	const int MAX_FILTER_COUNT = FILTER_NUMBER;
	//const int REG_COUNT = PROCESS_DATA_COUNT*MAX_FILTER_COUNT + FILTER_HEIGHT;
	const int WARP_COUNT = BLOCK_DIM_X >> 5;
	const int BLOCK_DATA_X = WARP_SIZE - FILTER_WIDTH + 1;
	const int BLOCK_DATA_Y = PROCESS_DATA_COUNT;
	const int DST_OFFSET = FILTER_WIDTH - 1;
	const int GRID_DIM_X = gridDim.x;
	const int GRID_DIM_Y = gridDim.y;
	assert(BLOCK_DIM_X == blockDim.x);
	//assert(REG_COUNT > FILTER_HEIGHT);
	//assert(blockDim.y == 1);
	const int laneId = threadIdx.x & 31;
	const int warpId = threadIdx.x >> 5;

	//assert(BLOCK_NUM_X == MAX_FILTER_COUNT);
	int BLOCK_IDX_X = blockIdx.x % BLOCK_NUM_X;
	int BLOCK_IDX_Y = blockIdx.x/ BLOCK_NUM_X;

	int tidx = laneId + BLOCK_DATA_X*BLOCK_IDX_X;
	int tidy = threadIdx.y + BLOCK_DATA_Y*blockIdx.y;

	__shared__ T smem[PROCESS_FILTER_COUNT][FILTER_CHANNELS][FILTER_HEIGHT][FILTER_WIDTH];
	T* psmem = &smem[0][0][0][0];

	const int filter_size = FILTER_WIDTH*FILTER_HEIGHT*FILTER_CHANNELS*PROCESS_FILTER_COUNT;
	const int offset = BLOCK_IDX_Y*filter_size;
	for (int i = threadIdx.x; i < filter_size; i += blockDim.x) {
		psmem[i] = __ldg(&filter[i + offset]);
	}
	//if (threadIdx.x < filter_size) {
	//	psmem[threadIdx.x] = filter[threadIdx.x + offset];
	//}
	__syncthreads();

	T data[FILTER_HEIGHT+PROCESS_DATA_COUNT-1];
	T result[PROCESS_FILTER_COUNT*PROCESS_DATA_COUNT];

	int index = tidx + widthStride*tidy + warpId*widthStride*height;
	#pragma unroll
	for (int s = 0; s < FILTER_HEIGHT + PROCESS_DATA_COUNT - 1; s++) {
		if (tidy + s < height && tidx < width)
			data[s] = __ldg(&src[index]);
		else
			data[s] = 0;
		index += widthStride;
	}
	T* pData = data;
	T* pResult = &result[0];

	const int PROCESS_COUNT = PROCESS_DATA_COUNT*PROCESS_FILTER_COUNT;
#pragma unroll
	for (int s = 0; s < PROCESS_FILTER_COUNT; s++) {
	//for (int s = 0; s < 1; s++) {
#pragma unroll
		for (int i = 0; i < PROCESS_DATA_COUNT; i++)
		{
			T sum = 0;
#pragma unroll
			for (int m = 0; m < FILTER_WIDTH; m++) {
				if (m > 0)
					sum = __shfl_up(sum, 1);
				//int a = data[0];
				//int b = data[1];
				//int c = data[2];
#pragma unroll
				for (int n = 0; n < FILTER_HEIGHT; n++) {
					int a = data[n + i];
					int b = data[n + i];
					int c = data[n + i];
					//sum += data[n + i] * smem[s][warpId][n][m];
					sum = MAD(data[n + i], smem[s][warpId][n][m], sum);
					//PTX_FMAD(data[i + n], smem[s][warpId][n][m], sum, sum);					
				}
			}
			result[s*PROCESS_DATA_COUNT+i] = sum;
			//result[i] = __shfl_down(sum, FILTER_WIDTH/2);
			//if (i + tidy < height) {
			//	dst[index] = sum;
			//	index += widthStride;
			//}
		}
	}

	__shared__ T sResult[PROCESS_COUNT][WARP_SIZE];

	//shared memory set 0
#pragma unroll
	for (int i = warpId; i < PROCESS_COUNT; i += WARP_COUNT) {
		sResult[i][laneId] = 0;
	}
	__syncthreads();

#pragma unroll
	for (int s = 0; s < PROCESS_COUNT; s++) {
		int idx = (s + warpId)% PROCESS_COUNT;
		//int a = result[idx];
		sResult[idx][laneId] += result[idx];
		__syncthreads();
	}

	//if (BLOCK_IDX_Y > 0 || tidy > 0) return;

	if (laneId < WARP_SIZE - DST_OFFSET && tidx < width/* && tidy < height */)
	{
		//index = tidx + widthStride*(tidy+warpId) + BLOCK_IDX_Y*widthStride*height;
#pragma unroll
		for (int i = warpId; i < PROCESS_COUNT; i += WARP_COUNT) {
			int iPos = i%PROCESS_DATA_COUNT;
			int iFlt  = i/ PROCESS_DATA_COUNT;
			if (iPos + tidy < height) {
				index = tidx + widthStride*(tidy + iPos) + (BLOCK_IDX_Y*PROCESS_FILTER_COUNT+iFlt)*widthStride*height;
				//index = tidx + widthStride*(tidy + iPos);
				dst[index] = sResult[i][laneId + DST_OFFSET];
			}
		}
	}
}

static void TestOneBlockOneChannel() {
	typedef float DataType;
	const int width = 514;
	const int height = width;
	const int channels = 3;

	const int filter_width = 3;
	const int filter_height = 3;
	const int filter_channels = channels;
	const int filter_number = 32;

	float inc = 0;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	//StopWatchWin watch;
	DataT<DataType> src, filter, dst;
	char szPath[1024] = "";
	src.MallocBuffer(width, height, channels, 1);
	filter.MallocBuffer(filter_width, filter_height, channels, filter_number);
	dst.MallocBuffer(width, height, filter.count, 1);

	for (int i = 0; i < src.Size(); i++) {
		//src.data[i] = i + 1;
		src.data[i] = 1;
	}
	for (int i = 0; i < filter.Size(); i++) {
		//filter.data[i] = i + 1;
		filter.data[i] = 1;
	}

	//sprintf(szPath, "../data/Lena%dx%d.raw", width, height);
	//bool bRtn = img.Load<uchar>(szPath, width, height);
	//if (!bRtn) printf("Load failed : %s\n", szPath);
	DevData<DataType> devSrc(src.width, src.height, src.channels), devDst(dst.width, dst.height, dst.channels), devFilter(filter.Size());
	devSrc.CopyFromHost(src.data, src.width, src.width, src.height, src.channels);
	devFilter.CopyFromHost(filter.data, filter.Size(), filter.Size(), 1);

	const int PROCESS_DATA_COUNT = 6;
	const int PROCESS_FILTER_COUNT = 1;
	const int FILTER_WIDTH = filter_width;
	const int FILTER_HEIGHT = filter_height;
	const int BLOCK_DIM_X = WARP_SIZE*channels;
	const int BLOCK_NUM_X = filter.count;

	dim3 block_size(BLOCK_DIM_X, 1);

	const int BLOCK_DATA_X = (BLOCK_DIM_X / WARP_SIZE * (WARP_SIZE - 2));

	dim3 grid_size(UpDivide(width - FILTER_WIDTH / 2 * 2, WARP_SIZE-FILTER_WIDTH/2*2)*BLOCK_NUM_X/ PROCESS_FILTER_COUNT, UpDivide(height, PROCESS_DATA_COUNT)/*-FILTER_HEIGHT/2*2*/);
	printf("%d %d, %d %d\n", grid_size.x, grid_size.y, block_size.x, block_size.y);
	cudaEventRecord(start, 0);
	kerenlOneBlockOneChannel<DataType, PROCESS_DATA_COUNT, PROCESS_FILTER_COUNT, FILTER_WIDTH, FILTER_HEIGHT, filter_channels, filter_number, BLOCK_DIM_X><<<grid_size, block_size>>>
		(grid_size.x/(BLOCK_NUM_X/ PROCESS_FILTER_COUNT), devSrc.GetData(), devDst.GetData(), width, devSrc.DataPitch(), height, channels, devFilter.GetData(), filter.width, filter.height, filter.channels, filter.count, filter.width/2, filter.height/2);
	cudaDeviceSynchronize();
	//watch.stop();
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	CUDA_CHECK_ERROR;

	devDst.CopyToHost(dst.data, dst.width, dst.width, dst.height, dst.channels);
	CUDA_CHECK_ERROR;

	cudaEventElapsedTime(&inc, start, stop);
	//inc = watch.getAverageTime();
	printf("%dx%dx%d %dx%dx%dx%d , %f ms , %f fps\n", width, height, channels, filter.width, filter.height, filter.channels, filter.count, inc, 1000.0 / inc);

	sprintf(szPath, "../data/data%dx%dx%dx.txt", dst.width, dst.height, dst.channels);
	dst.SaveText(szPath);
}

void Test_PipConv2_2_1() {
	TestOneBlockOneChannel();
}
