#ifndef __COMMON_H
#define __COMMON_H
#pragma once
#include <stdio.h>
#include <vector>

template<typename T>
struct ImageT {
	ImageT():data(NULL) {
	}
	inline ImageT& MallocBuffer(int w, int h) {
		_mem.resize(w*h);
		data = !_mem.empty() ? &_mem[0] : NULL;
		width = w;
		height = h;
		return *this;
	}
	inline bool Save(const char* szPath) {
		bool bRtn = false;
		if (data) {
			FILE* fp = fopen(szPath, "wb");
			if (fp) {
				bRtn = width*height == fwrite(data, sizeof(data[0]), width*height, fp) ? true : false;
				fclose(fp);
			}
		}
		return bRtn;
	}
	template<typename D>
	inline bool Load(const char* szPath, int w, int h) {
		bool bRtn = false;
		std::vector<D> vec(w*h);
		FILE* fp = fopen(szPath, "rb");
		if (fp) {
			if (w*h == fread(&vec[0], sizeof(vec[0]), w*h, fp)) {
				width = w;
				height = h;
				bRtn = true;
			}
			fclose(fp);
		}
		_mem.clear();
		_mem.resize(vec.size());
		if (bRtn) {
			width = w;
			height = h;
			for (int i = 0; i < vec.size(); i++)
				_mem[i] = vec[i];
		}
		data = !_mem.empty() ? &_mem[0] : NULL;
		return bRtn;
	}
	T* data;
	int width, height;
private:
	std::vector<T> _mem;
};


template<typename T>
struct DataT {
	DataT(int w = 0, int h = 0, int c = 0, int n = 0) :data(NULL), width(w), height(h), channels(c), count(n) {
		if (w*h*c*n > 0) MallocBuffer(w, h, c, n);
	}
	inline DataT& MallocBuffer(int w, int h, int c, int n) {
		_mem.resize(w*h*c*n);
		data = !_mem.empty() ? &_mem[0] : NULL;
		width = w;
		height = h;
		channels = c;
		count = n;
		return *this;
	}
	inline size_t Size() const {
		return width*height*channels*count;
	}
	inline bool SaveRaw(const char* szPath) {
		bool bRtn = false;
		if (data) {
			FILE* fp = fopen(szPath, "wb");
			if (fp) {
				bRtn = width*height*channels*count == fwrite(data, sizeof(data[0]), width*height*channels*count, fp) ? true : false;
				fclose(fp);
			}
		}
		return bRtn;
	}
	void SaveText(const char* szPath) {
		FILE* fp = fopen(szPath, "wt");
		if (fp) {
			int s = 0;
			fprintf(fp, "{\n");
			for (int i = 0; i < count; i++) {
				fprintf(fp, " {\n");
				for (int j = 0; j < channels; j++) {
					fprintf(fp, "  {\n");
					for (int m = 0; m < height; m++) {
						fprintf(fp, "   {");
						for (int n = 0; n < width; n++) {
							fprintf(fp, "%f ", float(data[s++]));
						}
						fprintf(fp, "   }\n");
					}
					fprintf(fp, "   }\n");
				}
				fprintf(fp, " }\n");
			}
			fprintf(fp, "}\n");
			fclose(fp);
		}
	}
	template<typename D>
	inline bool Load(const char* szPath, int w, int h, int c, int n) {
		bool bRtn = false;
		std::vector<D> vec(w*h*c*n);
		FILE* fp = fopen(szPath, "rb");
		if (fp) {
			if (w*h == fread(&vec[0], sizeof(vec[0]), w*h*c*n, fp)) {
				width = w;
				height = h;
				channels = c;
				count = n;
				bRtn = true;
			}
			fclose(fp);
		}
		_mem.clear();
		_mem.resize(vec.size());
		if (bRtn) {
			width = w;
			height = h;
			channels = c;
			count = n;
			for (int i = 0; i < vec.size(); i++)
				_mem[i] = vec[i];
		}
		data = !_mem.empty() ? &_mem[0] : NULL;
		return bRtn;
	}
	T* data;
	int width, height, channels, count;
private:
	std::vector<T> _mem;
};









#endif //__COMMON_H
